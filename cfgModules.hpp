#include "\AC\defines\commonDefines.inc"

/*
class Logic;
class Module_F: Logic;
{
	class AttributesBase
	{
		class Default;
		class Edit;
		class Combo;
		class Checkbox;
		class CheckboxNumber;
		class ModuleDescription;
		class Units;
	};
	class ModuleDescription;
};
*/

class AC_ModuleBase: Module_F
{
	scope = 1;
	displayName = "ModuleBase";
	icon = "\a3\Missions_F_Curator\data\img\iconMPTypeSectorControl_ca.paa";
	category = "AC_ArmaCommander";
	isGlobal = 2;
	isTriggerActivated = 0;
	isDisposable = 0;
	is3DEN = 1;

	class Attributes: AttributesBase
	{
		class ModuleDescription: ModuleDescription {};
	};

	class ModuleDescription: ModuleDescription
	{
		description = "Empty description.";
 	};
};

class AC_ModuleBattalion: AC_ModuleBase
{
	displayName = "Battalion";
	icon = "\a3\Modules_f\data\iconHQ_ca.paa";
	scope = 2;
	//function = "AC_fnc_moduleBattalion";

	class Attributes: AttributesBase
	{
		class Side: Combo
		{
			property = "AC_ModuleBattalion_Side";
			displayName = "Side";
			tooltip = "Which side the battalion belongs.";
			typeName = "NUMBER";
			defaultValue = "1";
			class Values
			{
				class WEST			{name = "WEST"; value = 1;};
				class EAST			{name = "EAST"; value = 0;};
				class RESISTANCE	{name = "INDEPENDENT"; value = 2;};
			};
		};

		class Points: Edit
		{
			property = "AC_ModuleBattalion_Points";
			displayName = "Requisition Points";
			tooltip = "How much resources battalion has from the start.";
			typeName = "NUMBER";
			defaultValue = "50";
		};

		class Type: Combo
		{
			property = "AC_ModuleBattalion_Type";
			displayName = "Type";
			tooltip = "What type of troops battalion consists.";
			typeName = "NUMBER";
			defaultValue = "0";
			class Values
			{
				class Militia_FIA		{name = "FIA Militia Battalion"; value = MILITIA_FIA;};
				class Recon_B			{name = "NATO Ranger Battalion"; value = RECON_B;};
				class Tank_B			{name = "NATO Mechanized Battlion"; value = TANK_B;};
				class Recon_O 			{name = "CSAT Recon Battalion"; value = RECON_O};
				class Infantry_O 		{name = "CSAT Guard Infantry Battalion"; value = INFANTRY_O};
				class Tank_O 			{name = "CSAT Tank Battalion"; value = TANK_O};
				class Mechanized_I		{name = "AAF Mechanized Battalion"; value = MECHANIZED_I};
				class Militia_Syndikat	{name = "Syndikat Militia Battalion"; value = MILITIA_SYNDIKAT;};
				class Custom			{name = "Custom Battalion"; value = CUSTOM;};
			};
		};
		class ModuleDescription: ModuleDescription{};
	};
	class ModuleDescription: ModuleDescription
	{
		description = "Battalion module. You can place two battalions into game as maximum.";
	};
};

class AC_ModuleACBase: AC_ModuleBase
{
	displayName = "Base";
	icon = "\a3\Modules_f\data\iconSector_ca.paa";
	scope = 2;

	canSetArea = 1;
	class AttributeValues
	{
		size3[] = {50, 50, -1};
	};

	class Attributes: AttributesBase
	{
		class Callsign: Edit
		{
			property = "AC_ModuleBattalion_Callsign";
			displayName = "Name";
			tooltip = "Name of the base (optional).";
			typeName = "STRING";
			defaultValue = """""";
		};

		class side: Combo
			{
			property = "AC_ModuleACBase_Side";
			displayName = "Side";
			tooltip = "Owner of the base";
			typeName = "NUMBER"; // Value type, can be "NUMBER", "STRING" or "BOOL"
			defaultValue = "1";
			class Values
			{
				class WEST			{name = "WEST"; value = 1;};
				class EAST			{name = "EAST"; value = 0;};
				class RESISTANCE	{name = "INDEPENDENT"; value = 2;};
				class EMPTY			{name = "EMPTY"; value = 8;};
			};
		};

		class nSoldiers: Edit
			{
			property = "AC_ModuleACBase_Garrison";
			displayName = "Garrison";
			tooltip = "Number of soldiers guarding the base.";
			typeName = "NUMBER"; // Value type, can be "NUMBER", "STRING" or "BOOL"
			defaultValue = "10"; // Default attribute value.
		};
		
		class value: Edit
		{
			property = "AC_ModuleBase_Value";
			displayName = "Value";
			tooltip = "How much Requisition Points this base give.";
			typeName = "NUMBER";
			defaultValue = "1";
		};

		class hidden: Combo
		{
			property = "AC_ModuleBase_Value";
			displayName = "Show to enemy";
			tooltip = "Show the base to all sides. Otherwise he has to find it first to see it.";
			typeName = "NUMBER";
			defaultValue = "0";
			class Values
			{
				class hide	{name = "Hide"; value = 1;};
				class show	{name = "Show"; value = 0;};
			};
		};

		class ModuleDescription: ModuleDescription{}; // Module description should be shown last
	};
	class ModuleDescription: ModuleDescription
	{
		description = "Base module. It can be any place ready to be defended."; // Short description, will be formatted as structured text
		sync[] = {"All"};
	};
};

class AC_ModuleAcGame: AC_ModuleBase
{
	displayName = "Arma Commander Game Mode";
	icon = "\A3\modules_f\data\iconStrategicMapMission_ca.paa";
	function = "AC_fnc_moduleAcGame";
	scope = 2;
	class Attributes: AttributesBase
	{
		class UnitCap: Edit
		{
			property = "AC_ModuleAcGame_UnitCap";
			displayName = "Maximum deployed groups";
			tooltip = "Set how many group from one battalion can be on the battlefield at one moment.";
			typeName = "NUMBER";
			defaultValue = "10";
		};

		class Length: Combo
		{
			property = "AC_ModuleAcGame_Length";
			displayName = "Scenario time";
			tooltip = "How long before before scenario ends.";
			typeName = "NUMBER";
			defaultValue = "3600";
			class Values
			{
				class L15	{name = "15 Minutes"; value = 900;};
				class L20	{name = "20 Minutes"; value = 1200;};
				class L30	{name = "30 Minutes"; value = 1800;};
				class L45	{name = "45 Minutes"; value = 2700;};
				class L60	{name = "1 Hour"; value = 3600;};
				class L90	{name = "1.5 Hours"; value = 5400;};
				class L120	{name = "2 Hours"; value = 7200;};
				class L360	{name = "10 Hours"; value = 36000;};
			};
		};

		class IncomeRate: Combo
		{
			property = "AC_ModuleAcGame_Income";
			displayName = "Income";
			tooltip = "How often will battalion recieve resources.";
			typeName = "NUMBER";
			defaultValue = "120";
			class Values
			{
				class VeryHigh	{name = "Every 30 seconds"; value = 30;};
				class High		{name = "Every minute"; value = 60;};
				class Medium	{name = "Every 2 minutes"; value = 120;};
				class Low		{name = "Every 4 minutes"; value = 340;};
				class VeryLow	{name = "Every 5 minutes"; value = 300;};
				class Min1		{name = "Every 8 minutes"; value = 420;};
				class Min2		{name = "Every 10 minutes"; value = 600;};
			};
		};

		class IncomeMultiplier: Edit
		{
			property = "AC_ModuleAcGame_Multiply";
			displayName = "Income Multiplier";
			tooltip = "How much should every base give player by default.";
			typeName = "NUMBER";
			defaultValue = "1";
		};

		class ModuleDescription: ModuleDescription{};
	};

	class ModuleDescription: ModuleDescription
	{
		description = "Game mode module - serves for setting scenario parameters.";
	};
};

class AC_ModuleRegisterGroup: AC_ModuleBase
{
	displayName = "Register Battalion Group";
	icon = "\A3\modules_f\data\iconStrategicMapMission_ca.paa";
	scope = 2;
	class Attributes: AttributesBase
	{
		class Name: Edit
		{
			displayName = "Name";
			property = "AC_ModuleRegisterGroup_Name";
			tooltip = "Name of the group shown in reuisition panel. Must be unique!";
			typeName = "STRING";
			defaultValue = """""";
		};
		class Tooltip: Edit
		{
			displayName = "Tooltip";
			property = "AC_ModuleRegisterGroup_Tooltip";
			tooltip = "Tooltip describing abilities of the group for the requisition panel.";
			typeName = "STRING";
			defaultValue = """""";
		};

		class Side: Combo
		{
			displayName = "Side";
			property = "AC_ModuleRegisterGroup_Side";
			tooltip = "How long before before scenario ends.";
			typeName = "NUMBER";
			defaultValue = "1";
			class Values
			{
				class West	{name = "BLUFOR"; 		value = 1;};
				class East	{name = "OPFOR"; 		value = 0;};
				class Indep	{name = "INDEPENDENT"; 	value = 2;};
			};
		};

		class Type: Combo
		{
			displayName = "Group Type";
			property = "AC_ModuleRegisterGroup_Type";
			tooltip = "Type of the group.";
			typeName = "NUMBER";
			defaultValue = "0";
			class Values
			{
				class Inf	{name = "Infantry";		value = TYPE_INFANTRY;};
				class Motor	{name = "Motorized"; 	value = TYPE_MOTORIZED;};
				class Arty	{name = "Artillery"; 	value = TYPE_ARTILLERY;};
			};
		};

		class Marker: Edit
		{
			displayName = "NATO Marker";
			property = "AC_ModuleRegisterGroup_Marker";
			tooltip = "Icon of the group. Any icon can be used, NATO markers are recommended.";
			typeName = "STRING";
			defaultValue = """\A3\ui_f\data\map\markers\nato\n_inf.paa""";
		};

		class Cost: Edit
		{
			displayName = "Cost";
			property = "AC_ModuleRegisterGroup_Cost";
			tooltip = "Cost of buying the group.";
			typeName = "NUMBER";
			defaultValue = "20";
		};
		class Count: Edit
		{
			displayName = "Count";
			property = "AC_ModuleRegisterGroup_Count";
			tooltip = "How many times the group can be requested.";
			typeName = "NUMBER";
			defaultValue = "4";
		};
		class Skill: Edit
		{
			displayName = "Skill";
			property = "AC_ModuleRegisterGroup_Skill";
			tooltip = "Overall skill of the group.";
			typeName = "NUMBER";
			defaultValue = "0.4";
		};
		class ModuleDescription: ModuleDescription{};
	};
	class ModuleDescription: ModuleDescription
	{
		description = "Adds group to Custom Battalion of specified side. Use it by synchronizing group leader to the module, group will then be available in requisition tab in the mission.";
	};
};

class AC_ModuleCustomBattalion: AC_ModuleBase
{
	displayName = "Create Custom Battalion";
	icon = "\A3\modules_f\data\iconStrategicMapMission_ca.paa";
	scope = 2;
	class Attributes: AttributesBase
	{
		class Side: Combo
		{
			displayName = "Side";
			property = "AC_ModuleCustomBat_Side";
			tooltip = "How long before before scenario ends.";
			typeName = "NUMBER";
			defaultValue = "1";
			class Values
			{
				class West	{name = "BLUFOR"; 		value = 1;};
				class East	{name = "OPFOR"; 		value = 0;};
				class Indep	{name = "INDEPENDENT"; 	value = 2;};
			};
		};

		class Faction: Edit
		{
			displayName = "Faction Name";
			property = "AC_ModuleCustomBat_Faction";
			tooltip = "Name of the group shown in reuisition panel. Must be unique!";
			typeName = "STRING";
			defaultValue = """""";
		};
		class Flag: Edit
		{
			displayName = "Battalion Flag";
			property = "AC_ModuleCustomBat_Flag";
			tooltip = "Flag texture that will fly on masts at your bases.";
			typeName = "STRING";
			defaultValue = """\a3\Data_F_Exp\Flags\flag_VIPER_CO.paa""";
		};
		class BatIcon: Edit
		{
			displayName = "Battalion Icon";
			property = "AC_ModuleCustomBat_BatIcon";
			tooltip = "Smaller square icon of the faction. You can find examples of these in CfgFactionClasses.";
			typeName = "STRING";
			defaultValue = """\a3\Data_F_Exp\FactionIcons\icon_VIPER_CA.paa""";
		};
		class Skill: Edit
		{
			displayName = "Base Defenders Skill";
			property = "AC_ModuleCustomBat_Skill";
			tooltip = "Smaller square icon .";
			typeName = "NUMBER";
			defaultValue = "0.4";
		};
		class ModuleDescription: ModuleDescription{};
	};
	class ModuleDescription: ModuleDescription
	{
		description = "Adds custom battalion, which can have any properties and units you want. To create groups for the battalion, register custom groups of the same side. n\To register base defenders, synchdonize place units you want into editor and sync them with this module.";
	};
};
