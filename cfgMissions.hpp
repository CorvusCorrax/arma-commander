class CfgMissions
{
	class MPMissions
	{
		class AC_maldenValley
		{
			briefingName = "Arma Commander: Malden South - Frontlines";
			overviewText = "Southern part of Malden turns into a battlefield.";
			directory = "AC\missions\AC_maldenValley.Malden";
		};
		class AC_maldenAirfield
		{
			briefingName = "Arma Commander: Malden Airfield - Invasion";
			overviewText = "NATO forces are attacking north of Malden.";
			directory = "AC\missions\AC_maldenAirfield.Malden";
		};
		class AC_maldenAirfield_Custom
		{
			briefingName = "Arma Commander: Custom Battalions (Malden Airfield)";
			overviewText = "Invasion on Malden with usage of custom battalion.";
			directory = "AC\missions\AC_maldenAirfield_Custom.Malden";
		};
	};
};

