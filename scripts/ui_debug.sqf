#include "\AC\defines\commonDefines.inc"

/*
	UI for debug that was not used for a long time, not sure if it works
	NOT LOADED AT START
*/

AC_debugUI = false;
AC_deb_allDebugMarkers = [];
AC_deb_index = 600;

AC_deb_UiController = {

	if (AC_debugUI) then {
		[] call AC_deb_createPlayerMarkers;
	};

	while {AC_debugUI} do {
		sleep 0.2;
		private _markersAlpha = markerAlpha (AC_deb_allDebugMarkers select 0);
		[] call AC_deb_updatePlayerMarkers;

		//show markers for current battle
		{
			private _debugMarkers = (_x getVariable "deb_markers");
			if (markerAlpha (_debugMarkers select 0) == 0) then {
				{
					_x setMarkerAlpha DEBUG_MARKER_ALPHA;
				} forEach _debugMarkers;
			};
		} forEach AC_battlesInProgress;

		//hide markers in ended battles
		{
			if (_x getVariable ["state",0] == 0 && {markerAlpha ((_x getVariable "deb_markers") select 0) > 0}) then {
				{
					_x setMarkerAlpha 0;
				} forEach (_x getVariable "deb_markers");
			};		
		} forEach AC_bases;

		//this is attempt for dynamic control of debug markers visibility
		/*
		if (AC_debugUI) then {
			//update all markers positions
			[] call AC_deb_updatePlayerMarkers;

			if (_markersAlpha == 0) then {
				{
					_x setMarkerAlpha DEBUG_MARKER_ALPHA;
				} forEach AC_deb_allDebugMarkers;
			};
		} else {
			//do not update markers, hide them
			if (_markersAlpha > 0) then {
				{
					_x setMarkerAlpha 0;
				} forEach AC_deb_allDebugMarkers;
			};
		};
		*/
	};
};

AC_deb_createPlayerMarkers = {
	private _playerUnit = 
	{
		private _size = SIMULATION_START_DISTANCE;
		private _color = "ColorBlack";
		if (_forEachIndex > 0) then {
			_size = SIMULATION_END_DISTANCE;
			_color = "ColorGrey";
		};
		private _mrk = createMarker [_x, getPosWorld player];
		_mrk setMarkerShape "ELLIPSE";
		_mrk setMarkerSize [_size,_size];
		_mrk setMarkerColor _color;
		_mrk setMarkerBrush "SolidBorder";
		_mrk setMarkerAlpha DEBUG_MARKER_ALPHA;
		
		AC_deb_allDebugMarkers pushBack _mrk;
	} forEach ["deb_mrk_1", "deb_mrk_2"];
};