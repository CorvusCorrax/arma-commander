#include "\AC\defines\commonDefines.inc"

// Every battalion has its own strategy agent
ACF_ai_strategyAgent = {
	while {!AC_ended} do {
		// Prepare data for strategy decisions
		{
			[_x] call ACF_ai_calculateDefensesAndThreat;
		} forEach AC_bases;

		{
			if (IS_AI_ENABLED(_x)) then {
				[_x] call ACF_ai_assignDefenders;
				[_x] call ACF_ai_assignAttacks;
				[_x] spawn ACF_assignIdleGroups;
			};
		} forEach AC_battalions;
		sleep STRATEGY_TICK;
	};
};

// Find out current defenses for the base - both their number and list of all units
ACF_ai_calculateDefensesAndThreat = {
	params ["_base"];
	private _side = GVAR(_base,"side");
	private _friendlyGroups = [_side] call ACF_combatGroups;

	private _defCurrentStr = 0;
	private _defAssignedStr = 0;

	//private _defCurrent = [group _base] + (_friendlyGroups select {_base distance2D leader _x < DEFENSE_DISTANCE}); // WRONG CALCULATION, this should count only current base units, nothing else
	private _defCurrent = [group _base];
	{
		_defCurrent pushbackUnique _x;
	} forEach (_friendlyGroups select {
		getWPPos [_x, currentWaypoint _x] distance2D _base < DEFENSE_DISTANCE 
		&& {getPosWorld leader _x distance2D _base < DEFENSE_DISTANCE}
	});

	private _defAssigned = +_defCurrent;
	{
		_defAssigned pushbackunique _x;
	} forEach (_friendlyGroups select {getWPPos [_x, currentWaypoint _x] distance2D _base < DEFENSE_DISTANCE});

	{
		_defCurrentStr = _defCurrentStr + ([_x] call ACF_ai_groupStrength);
	} forEach _defCurrent;

	{
		_defCurrentStr = _defCurrentStr + PLAYER_STRENGTH;
	} forEach (allPlayers select {private _p = _x; _defCurrent findIf {group _p == _x} > -1}); 

	{
		_defAssignedStr = _defAssignedStr + ([_x] call ACF_ai_groupStrength);
	} forEach _defAssigned;

	private _thrCurrentStr = 0;
	private _thrAssignedStr = 0;
	private _enemyGroups = AC_operationGroups select {side _x getFriend _side < 0.6};

	private _baseThreatDistance = THREAT_DISTANCE + GVAR(_base,"out_perimeter");
	private _thrCurrent = _enemyGroups select {_base distance2D leader _x < _baseThreatDistance};
	private _thrAssigned = +_thrCurrent;
	{
		_thrAssigned pushbackunique _x;
	} forEach (_enemyGroups select {getWPPos [_x, currentWaypoint _x] distance2D _base < DEFENSE_DISTANCE});

	{
		_thrCurrentStr = _thrCurrentStr + ([_x] call ACF_ai_groupStrength);
	} forEach _thrCurrent;
	{
		_thrAssignedStr = _thrAssignedStr + ([_x] call ACF_ai_groupStrength);
	} forEach _thrAssigned;

	{
		_thrCurrentStr = _thrCurrentStr + PLAYER_STRENGTH;
	} forEach (allPlayers select {private _p = _x; _thrCurrent findIf {group _p == _x} > -1}); 

	// What exacly is defenseCost? 
	private _attackCost = (_defCurrentStr + ((_defAssignedStr - _defCurrentStr) / 3)) * ATTACK_DEFENSE_RATIO + 2;
	private _defenseCost = (_thrCurrentStr + ((_thrAssignedStr - _thrCurrentStr) / 1.5)) * DEFENSIVE_RATIO - _defCurrentStr;

	_base setVariable ["att_cost",_attackCost]; // How "expensive" is to attack into a location at the moment
	_base setVariable ["def_cost",_defenseCost]; // How "expensive" defense is
	_base setVariable ["def_current",_defCurrent]; // What units are actually defending 
	_base setVariable ["def_currentStr",_defCurrentStr]; // How many units are assigned to defense of the base
	_base setVariable ["def_total",_defAssigned]; // What units are near base and what units are assigned
	_base setVariable ["def_totalStr",_defAssignedStr]; // Strength of those units
	_base setVariable ["thr_current",_thrCurrent]; // How many enemy units are around the base
	_base setVariable ["thr_currentStr",_thrCurrentStr];
	_base setVariable ["thr_total",_thrAssigned]; // Enemy units around the base and assigned to attack
	_base setVariable ["thr_totalStr",_thrAssignedStr];
};

// Debug hint of all base statuses
ACF_ai_showAiInfo = {
	private _hint = "Name: thrtC / thrtT / defC / defT / DEF \n";
	{
		private _str = format ["%1: AC: %2 /DC: %3 / %4\n", 
			_x getVariable "callsign",
			GVAR(_x,"att_cost"),			
			GVAR(_x,"def_cost"),
			GVARS(_x,"defended",true)
		];
		_hint = _hint + _str;
	} forEach AC_bases;

	// List of attacks
	_hint = _hint + "\n Attacks: \n";

	hintSilent format ["%1",_hint];
};

// Table of vehicle costs 
AC_vehicleString = [
	"O_APC_Tracked_02_cannon_F",
	"B_APC_Wheeled_01_cannon_F",
	"O_Truck_03_transport_F",
	"B_Truck_01_transport_F"
];
AC_vehicleStrength = [
	12,
	10,
	2,
	2
];

ACF_ai_groupStrength = {
	params ["_group"];
	private _result = {alive _x} count units _group;
	if (!isNull GVARS(_group,"base",objNull)) then {
		_result = leader _group getVariable ["nSoldiers",0];
	};

	{
		private _vehicleType = typeOf _x;
		private _vehicleStrength = 5; // default str of vehicle
		private _i = AC_vehicleString findIf {_vehicleType == _x};
		if (_i > -1) then {
			_vehicleStrength = AC_vehicleStrength#_i;
		};
		_result = _result + _vehicleStrength;
	} forEach ([_group] call ACF_getGroupVehicles);

	private _skillRatio = 2 * (skill leader _group);

	SVAR(_group, "str", _result * _skillRatio);
	_result
};

ACF_ai_groupsStrength = {
	params ["_groups"];
	private _totalStr = 0;
	{
		private _vehicleStr = 0; // default str of vehicle
		{
			private _i = AC_vehicleStr findIf {typeOf _x};
			if (_i > -1) then {
				_vehicleStr = _vehicleStr + (AC_vehicleStr#_i);
			} else {
				_vehicleStr = _vehicleStr + 5;
			};
		} forEach ([_group] call ACF_getGroupVehicles);
		_totalStr = _totalStr + ({alive _x} count units _group) + _vehicleStr;
	} forEach _groups;

	_totalStr
};

/*
	PART 2: Planning defense and offensives

	This is brain of the defense decisions. Its main task is to assign
	enough defenders to the front line bases:

	Deciding algorithm:

	- How important is to defend the base: Bases should be defended 
	according to their distance from FOB, manpower needed for defense, and avaliable troops

	Testcases:
		No problem: Nobody is attacking, all troops should be divided equally
		One base attack:
		Two-way attack:
		Losing side: Too many enemies attacking your front line
		Lost cause: Too many enemies everywhere, troops should make a last stand (and wait for reinforcements)

	Strategic calculations:

	1. Check if any units can be unassigned from defense
	2. Assign units for defense
	3. Check if offensive can be created: Check all enemy border bases

	---
	- counterattack as special defense function?
	- fn - ACF_findBorderBases; (all, friendly, enemy)
*/
ACF_ai_assignDefenders = {
	params ["_battalion"];
	private _side = GVARS(_battalion,"side",sideUnknown);
	private _bases = ([_side] call ACF_borderBases) select {GVAR(_x,"side") == _side};
	private _avaliableGroups = ([_battalion] call ACF_combatGroups) select {GVARS(_x,"canGetOrders",true)};
	private _basesPriority = [];
	{
		_basesPriority pushBack [_x, GVAR(_x,"def_cost")];
	} forEach _bases;

	private _basesConceded = [];
	private _conceded = [];
	{
		_x params ["_base","_threatStr"];
		if (_threatStr >= GVAR(_base,"def_currentStr") * RATIO_CONCEDE
			&& {count _bases - count _basesConceded > 1}
		) then {
			_basesConceded pushBack [_base,_threatStr];
			_conceded pushback _base;

			// DEBUG: Inform player on concede event
			if (GVAR(_base,"defended")) then {
				debugLog ("Base conceded: " + GVAR(_base,"callsign"));
				//playSound "Alarm";
			};

			SVAR(_base,"defended", false);
		} else {
			SVAR(_base,"defended", true);
		};
	} forEach _basesPriority;

	SVAR(_battalion,"basesConceded",_conceded);
	_basesPriority = _basesPriority - _basesConceded;

	{
		private _base = _x;
		// Free all assigned defenders
		{
			SVAR(_x,"canGetOrders",true);
			_avaliableGroups pushBackUnique _x;
		} forEach GVARS(_base,"def_total",[]);
	} forEach _conceded;

	// Sort bases according to their defensive priority
	_basesPriority sort false;

	// Assign nearest groups as long there are defenders needed or no more units avaliable
	{
		_x params ["_base","_score"];
		private _pos = getPosWorld _base;

		// Sort groups by distance
		_avaliableGroups = [_avaliableGroups,[],{_pos distance2D leader _x},"ASCEND"] call BIS_fnc_sortBy;
		{
			if (_score < 0) exitwith {};

			// Make sure that only units comming to real defense are actually assigned

			// If unit is too far, give it only soft assign, so it get better defense area
			private _canGetOrders = false;
			if (_pos distance2D leader _x > REINFORCEMENTS_RANGE_DEF) then {
				_canGetOrders = true;
			};

			private _wp = [_x, getPos _base, B_TRANSPORT, _canGetOrders,150] call ACF_ai_move;
			_wp setWaypointType "GUARD";
			
			_avaliableGroups deleteAt 0;
			_score = _score - ([_x] call ACF_ai_groupStrength);
		} forEach _avaliableGroups;
		if (count _avaliableGroups == 0) exitWith {};
	} forEach _basesPriority;
};

ACF_assignIdleGroups = {
	params ["_battalion"];
	private _side = GVAR(_battalion,"side");
	private _enemySide = [_side] call ACF_enemySide;
	private _groups = ([_battalion] call ACF_combatGroups) select {GVAR(_x,"canGetOrders") 
		&& {count waypoints _x == 0 
			|| {(getWPPos [_x, currentWaypoint _x])#0 == 0}
			|| {(getWPPos [_x, currentWaypoint _x]) distance leader _x < 50}
		};
	};
	private _frontlineBases = ([_side] call ACF_borderBases) select {GVAR(_x,"side") == _side};

	if (count _frontlineBases == 0) exitWith {};
	private _weights = _frontlineBases apply {{GVAR(_x,"side") == _enemySide} count GVAR(_x,"neighbors")};
	{
		private _base = _frontlineBases selectRandomWeighted _weights;
		if (!isNil "_base" && {!isNull _base}) then {
			[_x, getPos _base, B_TRANSPORT, true, 100] call ACF_ai_move;
		};
	} forEach _groups;
};

// This is function that takes care of attack procedures: How is the attack synchronized, what are the formations, etc.
ACF_ai_createOffensive = {
	params ["_base","_groups","_side"];
	[_base,_side,_groups] spawn ACF_ai_offensiveAgent;
};

ACF_ai_battleEnded = {
	params ["_base","_winner"];
	SVAR(_base,"defended",true);
	SVAR(_base,"attacked",sideUnknown);

	[_base] call ACF_ai_calculateDefensesAndThreat;
	private _aiSides = (AC_battalions select {GVARS(_x,"#commander","") == ""}) apply {GVAR(_x,"side")};
	{
		if (side _x in _aiSides) then {
			[_x, [_x] call ACF_rtbPos, B_COMBAT, true, 200] call ACF_ai_move;
		};
	} forEach (GVAR(_base,"def_total") + GVAR(_base,"thr_total"));
};