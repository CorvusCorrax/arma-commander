#include "\AC\defines\commonDefines.inc"

// TODO: Notifications for limited time

// Server side, right?
ACF_frontlinesModeAgent = {
	// 1. Give tasks for player
	sleep 5;

    [true,["task1"],["","Capture Enemy Bases",""],objNull,1,3,true] call BIS_fnc_taskCreate;

	// 2. Check victory conditions: Time, survival
	private _winner = sideUnknown;
	private _victoryType = "Minor";
	private _n5 = true;
	private _n1 = true;
	while {_winner == sideUnknown} do {
		sleep 1;

		// Side lost all bases
		{
			private _side = GVAR(_x,"side");
			if (AC_bases findIf {GVAR(_x,"side") == _side} == -1) exitWith {
				_winner = [_side] call ACF_enemySide;
				_victoryType = "Total";
			};
		} forEach AC_battalions;

		// Notifications:
		if (_n5 && {AC_endTimeServer <= 300}) then {
			SEND_NOTIFICATION(NN_TIME_REMAINING,5,0);
			_n5 = false;
		};
		if (_n1 && {AC_endTimeServer <= 60}) then {
			SEND_NOTIFICATION(NN_TIME_REMAINING,1,0);
			_n1 = false;
		};

		if (AC_endTimeServer <= 0) then {
			// Find out who won
			private _nBasesBattalion = [];
			{
				private _side = GVAR(_x,"side");
				_nBasesBattalion pushBack ({GVAR(_x,"side") == _side} count AC_bases);
			} forEach AC_battalions;

			if (_nBasesBattalion#0 != _nBasesBattalion#1) then {
				if (_nBasesBattalion#0 > _nBasesBattalion#1) then {
					_winner = GVAR(AC_battalions#0,"side");
				} else {
					_winner = GVAR(AC_battalions#1,"side");
				};

				// Find out type of victory
				_nBasesBattalion sort false;
				private _rate = _nBasesBattalion#0 / _nBasesBattalion#1;
				if (_rate > 1.5) then {
					_victoryType = "Major";
				};
			} else {
				_winner = sideEmpty; // Tie winner
			};
		};
	};

	// Select proper victory: Minor, major, total
	private _t = "";
	switch (_winner) do {
		case sideEmpty: {_t = "Tie"};
		case west: {
			if (_victoryType == "Minor") exitWith {_t = "WestMin"};
			if (_victoryType == "Major") exitWith {_t = "WestMaj"};
			if (_victoryType == "Total") exitWith {_t = "WestTot"};
		};
		case east: {
			if (_victoryType == "Minor") exitWith {_t = "EastMin"};
			if (_victoryType == "Major") exitWith {_t = "EastMaj"};
			if (_victoryType == "Total") exitWith {_t = "EastTot"};
		};
		case independent: {
			if (_victoryType == "Minor") exitWith {_t = "IndMin"};
			if (_victoryType == "Major") exitWith {_t = "IndMaj"};
			if (_victoryType == "Total") exitWith {_t = "IndTot"};
		};
	};
	[_t] call BIS_fnc_endMissionServer;
};

