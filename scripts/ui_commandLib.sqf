#include "\AC\defines\commonDefines.inc"

// Add functionality to each action button
ACF_ui_addMissionButtonHandlers = {
	disableserialization;
	private _table = UGVAR("ac_actionsList");
	((_table ctRowControls 0) select 2) ctrlAddEventHandler ["MouseButtonClick",{[B_TRANSPORT] call ACF_ai_moveAction}];
	((_table ctRowControls 1) select 2) ctrlAddEventHandler ["MouseButtonClick",{[B_COMBAT] call ACF_ai_moveAction}];
	((_table ctRowControls 2) select 2) ctrlAddEventHandler ["MouseButtonClick",{[B_SILENT] call ACF_ai_moveAction}];
	((_table ctRowControls 3) select 2) ctrlAddEventHandler ["MouseButtonClick",{[] call ACF_ai_vehicleAction}];
	((_table ctRowControls 4) select 2) ctrlAddEventHandler ["MouseButtonClick",{[] call ACF_ai_deployAction}];
	((_table ctRowControls 5) select 2) ctrlAddEventHandler ["MouseButtonClick",{[] call ACF_ai_fireMissionAction}];
};

// This controls button on battalion info - show or hide buying menu
ACF_ui_RequisitionButton = {
	disableSerialization;

	private _ctrl = UGVAR("ac_buyList");
	if (ctrlShown _ctrl) then {
		_ctrl ctrlShow false;
		AC_deployMode = false;
		[] call ACF_eraseSpawnZones;
		AC_markerPlaced = false;
	} else {
		_ctrl ctrlShow true;
		AC_deployMode = true;
		[] call ACF_drawSpawnZones;
	};
};

ACF_ai_moveAction = {
	params ["_type"];
	{
		[_x,_type] call ACF_ai_changeBehavior;
	} forEach AC_selectedGroups;
};

// Get in / unload / get out button
ACF_ai_vehicleAction = {
	private _group = AC_mouseOver;
	if (count AC_selectedGroups > 0) then {
		_group = AC_selectedGroups select 0;
	};

	// Determine type of action: Get in, get out, unload
	switch ([_group] call ACF_ui_vehicleActionType) do {
		case ACTION_GET_OUT: {
			// Create getOut WP on the spot
			[_group] call ACF_wp_getOut;
		};
		case ACTION_UNLOAD: {
			[_group] call ACF_wp_unload;
		};
		case ACTION_GET_IN: {
			[_group, leader _group, true] call ACF_wp_getIn;
		};
	};
};

ACF_ui_vehicleActionType = {
	params ["_group"];

	// Determine if action should be get in, get out, or unload
	private _vehicles = [];
	{
		if (vehicle _x != _x) then {
			_vehicles pushBackUnique vehicle _x;
		};
	} forEach units _group;
	private _transporting = false;

	{
		{
			if (alive _x && {group _x != _group}) exitWith {_transporting = true};
		} forEach crew _x;
	} forEach _vehicles;

	if (_transporting) exitWith {ACTION_UNLOAD};
	if (count _vehicles > 0 && {!isNull (_vehicles select 0)}) exitWith {ACTION_GET_OUT};
	ACTION_GET_IN
};

// Next valid click into space will be fire mission, if it's correct
ACF_ai_fireMissionAction = {
	params ["_group"];
	USVAR("fireMission",true);
};

// Immediately deploy/undeploy, switch cursor mode to valid move mode again
ACF_ai_deployAction = {
	debugLog "ACF_ai_deploy";
	if (count AC_selectedGroups == 0) exitWith {};
	private _group = AC_selectedGroups select 0;
	private _deployed = GVAR(_group,"sw_deployed");
	if (isNil "_deployed") exitWith {};
	private _staticVehicles = ([_group] call ACF_getGroupVehicles) select {_x isKindOf "StaticWeapon"};
	private _possibleBackpacks = ["B_Mortar_01_support_F","B_Mortar_01_weapon_F"];

	//TODO: Must be called on server, otherwise will not work!

	// Find state of deployment - offer action
	if (_deployed) then {

		// Undeploy action - put vehicles as backpacks on people without BPs, if possible
		private _backpacks = [];
		{
			/*
			switch (true) do {
				case (typeOf _x == "StaticMortar"): {_backpacks append ["B_Mortar_01_support_F","B_Mortar_01_weapon_F"]};
			};
			*/
			_backpacks = ["B_Mortar_01_support_F","B_Mortar_01_weapon_F"];
			debugLog str _backpacks;
	
			// save backpacks
			{moveOut _x} forEach crew _x;
			deleteVehicle _x;
		} forEach (([_group] call ACF_getGroupVehicles) select {_x isKindOf "StaticWeapon"});

		// Add backpacks on soldiers - just visual thingy
		{
			if (_forEachIndex >= count _backpacks) exitWith {};
			_x addBackpackGlobal (_backpacks select _forEachIndex);
		} forEach (units _group);
		SVARG(_group,"sw_deployed",false);
	} else {
		debugLog "Deploying";
		debugLog str _group;
		// Create static weapons and move in soldiers, remove backpacks
		{
			private _weapon = createVehicle [_x, getPosWorld leader _group];

			// Move soldier into the thingy
			// BUG: MoveInGunner create weird glitch
			private _unitsOutsideVehicle = units _group select {vehicle _x == _x};
			if (count _unitsOutsideVehicle > 0) then {
				private _gunner = _unitsOutsideVehicle select 0;
				_gunner assignAsGunner _weapon;
				[_gunner] orderGetIn true;
			};
		} forEach _staticVehicles;

		{
			if (_possibleBackpacks find (backpack _x) > -1) then {
				removeBackpackGlobal _x;
			};
		} forEach units _group;
		SVARG(_group,"sw_deployed",true);
	};
	[] call ACF_ui_updateActionsList;
};