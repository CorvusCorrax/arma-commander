#include "\AC\defines\commonDefines.inc"

// Simulation macros
#define SPAWN_RANGE_PLAYER 600
#define DESPAWN_RANGE_PLAYER (SPAWN_RANGE_PLAYER + 100)
#define SPAWN_RANGE_AI 300
#define DESPAWN_RANGE_AI (SPAWN_RANGE_AI + 100)

// Only base simulation is calculated now
ACF_checkSimulation = {
	{
		private _base = _x;
		private _pos = getPosWorld _x;
		private _enemySide = [GVAR(_x,"side")] call ACF_enemySide;
		private _deployed = GVAR(_x,"deployed");
		private _forceDeployed = GVARS(_x,"forcedDeploy",false);

		if (_deployed == DEPLOYED_FALSE && !_forceDeployed) then {
			if (allPlayers findIf {_x distance _pos <= SPAWN_RANGE_PLAYER} > -1
				|| {(_pos nearEntities [["Man", "AllVehicles"], SPAWN_RANGE_AI]) findIf {side _x == _enemySide} > -1} 
			) exitWith {
				[_x] spawn AC_gar_deployGarrison;
			};
		};

		if (_deployed == DEPLOYED_TRUE && !_forceDeployed) then {
			if (allPlayers findIf {_x distance _pos <= DESPAWN_RANGE_PLAYER} == -1
				&& {(_pos nearEntities [["Man", "AllVehicles"], DESPAWN_RANGE_AI]) findIf {side _x == _enemySide} == -1} 
			) exitWith {
				[_x] spawn AC_gar_undeployGarrison;
			};
		};
	} forEach AC_bases;
};

// Detection macros
#define DETECTION_TRESHOLD 3.5
#define FORGET_TRESHOLD 3.1
#define DETECTION_DISTANCE 800
#define TIME_TO_FORGET 90

// Hidden bases detection. Note: Empty bases cannot be hidden!
ACF_baseDetectionCheck = {
	{
		private _base = _x;
		private _oponentSide = [GVAR(_x,"side")] call ACF_enemySide;
		private _deployedUnits = (units GVARS(_x,"staticGroup",grpNull)) + (units GVARS(_x,"dynamicGroup",grpNull));
		{
			if (_oponentSide knowsAbout vehicle _x > DETECTION_TRESHOLD && {GVAR(_x,"deployed" == DEPLOYED_TRUE)}) exitWith {
				SVARG(_base,"detected",true);
				[_x] remoteExec ["ACF_ui_revealGroup",_oponentSide];
			};
		} forEach _deployedUnits;
	} forEach (AC_bases select {!GVARS(_x,"detected",true) && GVAR(_x,"side")!= sideEmpty});
};

// New group detection check as replacement of basic behavior:
ACF_groupdetectionCheck = {
	{
		private _ownSide = GVAR(_x,"side");
		private _enemySide = [_ownSide] call ACF_enemySide;
		private _ownGroups = AC_operationGroups select {side _x == _ownSide};
		private _enemyGroups = AC_operationGroups select {side _x == _enemySide};

		{
			private _group = _x;
			private _leader = vehicle leader _x;
			private _nearBases = _leader nearEntities ["AC_ModuleACBase",DETECTION_DISTANCE];
			private _nearEnemyGroups = _enemyGroups select {leader _x distance _leader < DETECTION_DISTANCE};
			{
				if (GVAR(_x,"deployed") == DEPLOYED_TRUE) then {
					_nearEnemyGroups append [GVAR(_x,"staticGroup"),GVAR(_x,"dynamicGroup")];
				};
			} forEach _nearBases;

			// Check for detection
			if (!GVARS(_group,"detected",false)) then {
				{
					//if ((leader _x targetKnowledge _leader)#0 && {_x knowsAbout _leader > 3}) exitWith {
					if (_x knowsAbout _leader > 3) exitWith {
						SVARG(_group,"detected",true);
						SVAR(_group,"detectedTime",time);
						[_group] remoteExec ["ACF_ui_revealGroup",_enemySide];
						//systemChat format ["%1 Revealed!", GVAR(_group,"callsign")];
					};
				} forEach _nearEnemyGroups;
			} else {
				if (GVARS(_x,"detectedTime",0) < time - TIME_TO_FORGET) then {
					private _forgotten = true;
					{
						if (_x knowsAbout _leader > 3) exitWith {
							_forgotten = false;
						};
					} forEach _nearEnemyGroups;
					if (_forgotten) then {
						SVARG(_group,"detected",false);
						//systemChat format ["%1 Forgotten", GVAR(_group,"callsign")];
					};
				};
			};
		} forEach _ownGroups;
	} forEach AC_battalions;
};
