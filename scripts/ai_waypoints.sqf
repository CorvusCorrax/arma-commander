#include "\AC\defines\commonDefines.inc"

AC_battlesInProgress = [];

ACF_ai_move = {
	params ["_group","_pos",["_mode",B_DEFAULT],"_canGetOrders",["_radius",0]];
	if (_mode != B_DEFAULT) then {
		[_group,_mode] call ACF_ai_changeBehavior;
	};

	// Delete all waypoints
	while {count waypoints _group > 0} do {
		deleteWaypoint ((waypoints _group)#0);
	};

	if (_pos isEqualType [] && {_radius > 0}) then {
		_pos = [_pos,_radius] call ACF_randomPos;
	};

	if (!isNil "_canGetOrders") then {
		SVAR(_group,"canGetOrders",_canGetOrders);
	};

	//SVAR(_group,"#wp",_pos);
	private _wp = _group addWaypoint [_pos, 0];
	_wp
};

ACF_rtbPos = {
	params ["_group"];
	private _side = side _group;
	private _pos = getPos leader _group;
	private _basePos = _pos;

	// Find nearest friendly base
	if (AC_bases findIf {GVAR(_x,"side") == _side} > -1) then {
		private _friendlyBases = [];
		private _range = 500;
		while {_range < 5000} do {
			_friendlyBases = _pos nearEntities ["AC_moduleAcBase",_range];
			if (count _friendlyBases > 0) exitWith {
				_friendlyBases = [_friendlyBases,[],{_x distance2D _pos},"ASCEND"] call BIS_fnc_sortBy;
				_basePos = getPos (_friendlyBases#0);
			};
			_range = _range + 500;
		};
	};
	_basePos
};

ACF_ai_changeBehavior = {
	params ["_group","_behavior"];
	private _units = units _group;

	// Set specific behaviour parts
	switch (_behavior) do {
		case B_COMBAT: {
			_group setCombatMode "YELLOW";
			_group setSpeedMode "NORMAL";
			{_x enableAI "autocombat"} forEach _units;
		};
		case B_TRANSPORT: {
			_group setCombatMode "YELLOW";
			_group setBehaviour "AWARE";
			_group setSpeedMode "FULL";
			{_x disableAI "autocombat"} forEach _units;
		};
		case B_SILENT: {
			_group setBehaviour "STEALTH";
			_group setCombatMode "Green";
			_group setSpeedMode "LIMITED";
			{_x limitSpeed (SPEED_LIMIT * 0.6)} forEach ([_group] call ACF_getGroupVehicles);
			{_x enableAI "autocombat"} forEach _units;
		};
	};
	SVAR(_group,"#b",_behavior);
};

// TODO: Make the function more universal
AC_ai_fireMission = {
	params ["_group","_pos",["_spread",50],["_nRounds",4],["_sender",objNull]];
	private _artillery = vehicle leader _group;
	private _types = (getArtilleryAmmo [_artillery]);
	if (count _types == 0) exitWith {playSound "3DEN_notificationWarning"};
	private _roundType = _types#0;

	if (_pos isEqualType objNull) then {
		_pos = getPosWorld _pos;
	};
	private _roundPos = [_pos, _spread] call ACF_randomPos;

	// Check if target can be hit
	private _eta = _artillery getArtilleryETA [_roundPos, (getArtilleryAmmo [_artillery])#0];;
	if (_eta == -1) exitWith {playSound "3DEN_notificationWarning"};

	// Create simulation zone, a small one
	doStop _artillery;
	private _simulationZone = (group ((entities "logic") select 0)) createUnit ["LOGIC",_pos , [], 0, ""];
	_simulationZone setVariable ["simulationRange", 300];
	AC_battlesInProgress pushBack _simulationZone;

	if (!isNull _sender) then {
		if (_eta > -1) then {
			[_eta,_pos,_spread + 15] remoteExec ["ACF_showFireZone", _sender];
		} else {
			"3DEN_notificationWarning" remoteExec ["playSound",_sender];
		};
	};
	_artillery commandArtilleryFire [_roundPos, _roundType, 4];
	sleep (_eta + 10);
	AC_battlesInProgress = AC_battlesInProgress - [_simulationZone];
	deleteVehicle _simulationZone;
};

ACF_showFireZone = {
	params ["_eta","_pos","_spread"];

	// Prepare marker
	playSound "FD_Finish_F";
	private _mrk = createMarkerLocal [str _pos,_pos];
	_mrk setMarkerShapeLocal "ELLIPSE";
	_mrk setMarkerSizeLocal [_spread,_spread];
	_mrk setMarkerColorLocal "colorBlack";
	_mrk setMarkerColorLocal "colorBlack";
	_mrk setMarkerAlphaLocal 0.5;
	sleep (_eta + 10);
	deleteMarkerLocal _mrk;
};

ACF_wp_getIn = {
	params ["_group","_target",["_auto",false]];
	deleteWaypoint [_group, 0];

	// Target must be position or object
	if (_target isEqualType grpNull) then {
		_target = vehicle leader _target;
	};

	// Add or edit waypoint
	deleteWaypoint [_group, 0];
	private _wp = _group addWaypoint [getPos _target, 0,0];

	if (_auto) then {
		_wp setWaypointType "GETIN NEAREST";
	} else {
		_wp waypointAttachVehicle _target;
		_wp setWaypointType "GETIN";
		// Track the vehicle
		[_group, _target] spawn {
			params ["_group","_target"];

			while {alive _target && waypointType [_group,0] == "GETIN"} do {
				sleep 1;
				[_group,0] setWPPos getPos _target;
			};
		};
	};
};

ACF_wp_unload = {
	params ["_group"];
	private _loadedGroups = [];
	private _groupVehicles = [];
	{
		if (vehicle _x != _x && {group effectiveCommander vehicle _x == _group}) then {
			_groupVehicles pushbackunique _x;
		};
	} forEach units _group;

	// Find people in group's vehicle(s), and force them out
	{
		{
			if (group _x != _group) then {
				_loadedGroups pushBackUnique group _x;
			};
		} forEach crew _x;
	} forEach _groupVehicles;
	{[_x] call ACF_wp_getOut} forEach _loadedGroups;
	doStop units _group;
};

ACF_wp_getOut = {
	params ["_group"];
	deleteWaypoint [_group, 0];

	// Target must be position or object
	if (_target isEqualType grpNull) then {
		_target = vehicle leader _target;
	};

	deleteWaypoint [_group, 1];
	private _wp = _group addWaypoint [getPos vehicle leader _group, 0,1];
	_wp setWaypointType "GETOUT";
};

