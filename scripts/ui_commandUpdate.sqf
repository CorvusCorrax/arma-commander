#include "\AC\defines\commonDefines.inc"

/* 
	UI updates:
	Functions for updating whole UI and all its parts.
*/

// Update each visible UI element
ACF_ui_updateCommandUI = {
	if (!visibleMap) exitWith {};
	private _object = AC_mouseOver;
	if (count AC_selectedGroups > 0) then {_object = AC_selectedGroups select 0};
	private _updateFunctions = [ACF_ui_updateActionsList, ACF_ui_updateGroupInfo, ACF_ui_updateBattalionInfo, ACF_ui_updateBuyList, ACF_ui_updateBaseInfo];
	{
		if (ctrlShown UGVAR(_x)) then {
			[_object] call (_updateFunctions#_forEachIndex);
		};
	} forEach ["ac_actionsList", "ac_groupInfo", "ac_battalionInfo", "ac_buyList", "ac_baseInfo"];
};

ACF_ui_updateBaseInfo = {
	params ["_object"];
	disableSerialization;
	private _base = leader _object;

	private _side = GVARS(_base,"side",sideUnknown);
	private _table = UGVAR("ac_baseInfo");
	private _color = [_side] call BIS_fnc_sideColor;
	//private _ammo = "? supplies";
	private _soldiers = "? soldiers";
	if (_side == AC_playerSide) then {
		_ammo = format ["%1 Supplies", (_base getVariable ["supplies",999])];
		_soldiers = format ["%1 Soldiers", (_base getVariable ["nSoldiers",999])];
	};

	private _soldiersColor = [GVAR(_base,"nSoldiers"), GVAR(_base,"nSoldiersOriginal")] call ACF_ui_depletionColor;

	private _percent = "";
	private _percentColor = [1,1,1,1];
	if (GVAR(_base,"contested")) then {
		private _score = GVARS(_base,"score",0);
		_percent = format ["%1%2",abs _score,"%"];

		if (_side == sideUnknown || _side == sideEmpty) then {
			if (_score > 0) then {
				_percentColor = [GVAR(AC_battalions#0,"side")] call BIS_fnc_sideColor;
			};
			if (_score < 0) then {
				_percentColor = [GVAR(AC_battalions#1,"side")] call BIS_fnc_sideColor;
			};
		} else {
			if (_score < 0) then {
				_percentColor = [[_side] call ACF_enemySide] call BIS_fnc_sideColor;
			};
		};

	};

	//Info: Color, callsign, supplies, soldiers, resupply option, reinforceOption (dis)
	((_table ctHeaderControls 0)#0) ctrlSetBackgroundColor _color;
	((_table ctHeaderControls 1)# 0) ctrlSetBackgroundColor _color;
	((_table ctHeaderControls 0)# 2) ctrlSetText GVAR(_base,"callsign");

	((_table ctHeaderControls 1)# 3) ctrlSetTextColor _percentColor;
	((_table ctHeaderControls 1)# 3) ctrlSetText _percent;

	((_table ctRowControls 0)# 2) ctrlSetText _soldiers;
	((_table ctRowControls 0)# 2) ctrlSetTextColor _soldiersColor;
};

// Shows selected unit, if there is none, show hovered unit
ACF_ui_updateGroupInfo = {
	params ["_group"];
	disableSerialization;

	private _side = side _group;
	private _table = UGVAR("ac_groupInfo");

	// Show info, update all variable fields
	private _sideColor = [side _group] call BIS_fnc_sideColor;
	private _groupName = _group getVariable ["callsign", ""];
	private _rankIcon = [_group getVariable ["skill",0.4]] call ACF_ui_getRankIcon;

	// Show type of the unit
	private _groupType = [GVARS(_group,"typeStr",""),"name"] call ACF_getGroupString;

	private _ammo = "? ammo";
	private _soldiers = "? soldiers";
	private _vehicles = "? vehicles";
	private _soldiersColor = [1,1,1,1];
	private _vehicleColor = [1,1,1,1];

	// Show extra info for friendly units
	if (_side == AC_playerSide) then {
		private _nUnits = {alive _x} count units _group;
		private _nVehicles = count ([_group] call ACF_getGroupVehicles);
		_ammo = format ["%1%2 ammo",(_group getVariable ["ammo", 50]) * 100,"%"];
		_soldiers = format ["%1 soldiers",_nUnits];
		_vehicles = format ["%1 vehicles",_nVehicles];
		_soldiersColor = [_nUnits, count ([GVAR(_group,"typeStr"),"units"] call ACF_getGroupArray)] call ACF_ui_depletionColor;
		_vehicleColor = [ _nVehicles, count ([GVAR(_group,"typeStr"),"vehicles"] call ACF_getGroupArray)] call ACF_ui_depletionColor;
	};

	// Update ctrls
	private _headerBackground1 = (_table ctHeaderControls 0) select 0;
	private _headerBackground2 = (_table ctHeaderControls 1) select 0;
	_headerBackground1 ctrlSetBackgroundColor _sideColor;
	_headerBackground2 ctrlSetBackgroundColor _sideColor;

	{
		private _row = _table ctHeaderControls _forEachIndex;
		private _headerTextCtrl = _row select 2;
		private _headerColorCtrl = _row select 0;

		_headerColorCtrl ctrlSetBackgroundColor _sideColor;
		_headerTextCtrl ctrlSetText _x;
	} forEach [_groupName, _groupType];
	private _headerRank = (_table ctHeaderControls 1) select 1;

	_headerRank ctrlSetText _rankIcon;

	{
		private _textCtrl = (_table ctRowControls _forEachIndex) select 2;
		_textCtrl ctrlSetText _x;
	} forEach [_soldiers,_vehicles];

	{
		private _textCtrl = (_table ctRowControls _forEachIndex) select 2;
		_textCtrl ctrlSetTextColor _x;
	} forEach [_soldiersColor,_vehicleColor];

	// Check if unit can be reinforced:
	private _resupplyCost = [_group,AC_playerBattalion] call ACF_canResupply;	
	private _button = (_table ctRowControls 0)#3;

	private _tooltip = "";
	if (_resupplyCost > -1 && {GVAR(AC_playerBattalion,"points") >= _resupplyCost}) then {
		_button ctrlEnable true;
		_tooltip = format ["Resupply for %1 points. \n\Available only when group is out of combat",_resupplyCost];
	} else {
		_button ctrlEnable false;
	};
	_button ctrlSetTooltip _tooltip;

	// Check if unit can be switched
	private _switchButton = (_table ctHeaderControls 1) select 3;

	if (_side == AC_playerSide
		&& {GVAR(_group,"type") != TYPE_ARTILLERY}
		&& {_group != group player}
	) then {
		_switchButton ctrlEnable true;
	} else {
		_switchButton ctrlEnable false;
	};
};

ACF_ui_updateBattalionInfo = {
	disableSerialization;
	(UGVAR("ac_battalionInfo") ctRowControls 0) params ["_background","_icon","_points","_income","_requisitionButton"];

	private _countdown = 0;
	if (!isNil "AC_nextIncomeTime") then {
		_countdown = AC_nextIncomeTime - time;
	};

	private _ownedBases = {GVAR(_x,"side") == AC_playerSide} count AC_bases;
	_points ctrlSetText (str (floor GVAR(AC_playerBattalion,"points")));
	_income ctrlSetText format ["| +%1 in %2 |", floor (_ownedBases * GVAR(AC_gameModule,"IncomeMultiplier")), [_countdown] call ACF_timer];

	// Enable or disable requisition button depending on player being commander
	if (ctrlEnabled _requisitionButton && {!IS_COMMANDER}) then {
		_requisitionButton ctrlEnable false;
	};
	if (!ctrlEnabled _requisitionButton && {IS_COMMANDER}) then {
		_requisitionButton ctrlEnable true;
	};
};

ACF_ui_updateBuyList = {
	disableSerialization;
	private _table = UGVAR("ac_buyList");
	private _rp = GVAR(AC_playerBattalion,"points");
	private _nGroups = GVAR(AC_playerBattalion,"nActiveGroups");
	private _points = GVAR(AC_playerBattalion,"points");

	private _unitList = [AC_playerBattalion] call ACF_ec_unitTable;

	// Header: Update unit count
	(_table ctHeaderControls 0)#3 ctrlSetText format ["%1/%2",_nGroups, AC_unitCap];

	for "_i" from 0 to (ctRowCount _table) - 1 do {
		(_unitList#_i) params ["_type","_count","_cost","_originalCount"];

		private _unitCount = (_table ctRowControls _i) select 1;
		_unitCount ctrlSetText format ["%1/%2",_count,_originalCount];

		private _rowButton = (_table ctRowControls _i) select 5;
		if (_points < _cost || {_nGroups >= AC_unitCap} || _count <= 0) then {
			_rowButton ctrlEnable false;
		} else {
			_rowButton ctrlEnable true;
		};
	};
};

ACF_ui_updateScoreInfo = {
	disableSerialization;
	private _table = UGVAR("AC_scoreInfo");
	private _timeToEnd = [MGVARS("AC_endTime",1000), "HH:MM:SS"] call BIS_fnc_secondsToString;
	private _side1 = GVAR(AC_battalions#0,"side");
	private _nBases1 = {GVAR(_x,"side") == _side1} count AC_bases;
	private _side2 = GVAR(AC_battalions#1,"side");
	private _nBases2 = {GVAR(_x,"side") == _side2} count AC_bases;

	// Default values because some trouble with initialization at start
	(_table ctHeaderControls 0) params ["_background",["_flag1",""],["_text1",""],["_timer",""],["_text2",""]];
	_text1 ctrlSetText str _nBases1;
	_text2 ctrlSetText str _nBases2;
	_timer ctrlSetText _timeToEnd;
};

ACF_ui_updateActionsList = {
	params ["_group"];
	private _table = UGVAR("ac_actionsList");
	private _deployButton = (_table ctRowControls 4) select 2;
	private _fireButton = (_table ctRowControls 5) select 2;
	private _transportButton = (_table ctRowControls 3) select 2;

	// Determine get in, get out, unload text:
	private _text = "GET IN";
	switch ([_group] call ACF_ui_vehicleActionType) do {
		case ACTION_GET_OUT: {_text = "GET OUT"};
		case ACTION_UNLOAD: {_text = "UNLOAD TRANSPORT"};
	};

	if (ctrlText _transportButton != _text) then {
		_transportButton ctrlSetText _text;
	};

	// Handle fire button
	if (AC_selectedGroups findIf {GVAR(_x,"type") == TYPE_ARTILLERY} > -1) then {
		_fireButton ctrlEnable true;
	} else {
		_fireButton ctrlEnable false;
	};

	_table ctSetCurSel ([] call ACF_ui_highlightedAction);

	// Deploy and undeploy is temporarly disabled!
	_deployButton ctrlEnable false;
};

ACF_ui_highlightedAction = {
	if (count AC_selectedGroups == 0) exitWith {-1};
	private _firstSelected = AC_selectedGroups#0;
	private _index = -1;
	if (UGVARS("fireMission",false)) exitWith {5};

	// Find out mode of first unit and find out if this works across the board
	switch (true) do {
		case (AC_selectedGroups findIf {GVARS(_group,"#b",B_DEFAULT) != B_TRANSPORT} == -1): {
			_index = 0;
		};
		case (AC_selectedGroups findIf {GVARS(_group,"#b",B_DEFAULT) != B_COMBAT && GVARS(_group,"#b",B_DEFAULT) != B_DEFAULT} == -1): {
			_index = 1;
		};
		case (AC_selectedGroups findIf {GVARS(_group,"#b",B_DEFAULT) != B_SILENT} == -1): {
			_index = 2;
		};
	};
	_index
};