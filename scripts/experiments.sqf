#include "\AC\defines\commonDefines.inc"

AC_dropzone = "";
AC_deployMarkers = [];
AC_deployMode = false;
AC_markerPlaced = false;

#define DEPLOY_RADIUS 350
#define ENEMY_DISTANCE 1000

ACF_drawSpawnZones = {
	private _basesToSpawn = [AC_playerSide] call ACF_findSpawnBases;
	if (count AC_deployMarkers > 0) exitWith {};
	setMousePosition [0.5,0.5];

	// Draw local markers around the areas
	{
		// Draw areas around the bases, so you are able to spawn 
		private _mrk = createMarkerLocal [format ["sz%1",_forEachIndex],getPosWorld _x];
		_mrk setmarkershapelocal "ELLIPSE";
		_mrk setmarkertypelocal "Solid";
		_mrk setmarkersizelocal [DEPLOY_RADIUS,DEPLOY_RADIUS];
		_mrk setmarkercolorlocal "colorCivilian";
		_mrk setmarkeralphalocal 0.3;

		AC_deployMarkers pushback _mrk;
	} forEach _basesToSpawn;

	// Move cursor to middle of screen

	if (AC_dropzone == "") then {
		AC_dropzone = createMarkerLocal ["dropZone", UGVAR("#map") posScreenToWorld getMousePosition];
		AC_dropzone setmarkershapelocal "ICON";
		AC_dropzone setMarkerTypeLocal "mil_end";
		AC_dropzone setmarkercolorlocal "ColorGreen";
		AC_dropzone setmarkeralphalocal 1;
		AC_dropzone setmarkerTextlocal "Landing Zone";
	};
};

ACF_findSpawnBases = {
	params ["_side"];
	private _enemySide = [_side] call ACF_enemySide;
	private _enemyBases = AC_bases select {GVAR(_x,"side") == _enemySide};
	private _enemyGroups = AC_operationGroups select {side _x == _enemySide && GVARS(_x,"detected",false)};
	private _basesToSpawn = AC_bases select {private _base = _x;
		GVAR(_x,"side") == _side
		&& {_enemyGroups findIf {_base distance (leader _x) < ENEMY_DISTANCE} == -1}
		&& {_enemyBases findIf {_base distance (leader _x) < ENEMY_DISTANCE} == -1}
	};

	// Backup - make at least one base deployable
	if (count _basesToSpawn == 0) then {
		_basesToSpawn = AC_bases select {GVAR(_x,"side") == _side};
	};
	_basesToSpawn
};

ACF_eraseSpawnZones = {
	deleteMarkerLocal AC_dropzone;
	AC_dropzone = "";
	{
		deleteMarkerLocal _x;	
	} forEach AC_deployMarkers;
	AC_deployMarkers = [];
};

[] spawn {
	waitUntil {time > 0 && !isNull (findDisplay 12)};

	// Place dropzone marker
	(findDisplay 12 displayctrl 51) ctrlAddEventHandler ["MouseButtonClick",{
		if (!AC_markerPlaced && AC_deployMode) then {
			private _mousePosWorld = UGVAR("#map") posScreenToWorld getMousePosition;
			if (AC_deployMarkers findIf {getMarkerPos _x distance2D _mousePosWorld <= DEPLOY_RADIUS} > -1) then {
				AC_markerPlaced = true;
				playSound "addItemOk";
			} else {
				playSound "addItemFailed";
			};
		};
	}];

	(findDisplay 12 displayctrl 51) ctrlAddEventHandler ["Draw",{
		if (AC_deployMode && !AC_markerPlaced) then {
			AC_dropzone setmarkerposlocal (UGVAR("#map") posScreenToWorld getMousePosition);
		};
	}];

	addMissionEventHandler ["Map", {
		params ["_opened"];
		if (!_opened) then {
			[] call ACF_eraseSpawnZones;
			AC_deployMode = false;
			AC_markerPlaced = false;
		};
	}];
};

// New battle flow
ACF_checkBattles = {
	private _iBattle = AC_bases findIf {
		private _baseSide = GVAR(_x,"side");
		!GVARS(_x,"contested",false)
		&& {(_x nearEntities [["Man","LandVehicle"],GVAR(_x,"out_perimeter")]) findIf {side _x in ([west, east, independent] - [_baseSide])} > -1}
	};

	if (_iBattle > -1) then {
		[AC_bases#_iBattle] spawn ACF_battleAgent;
	};
};

// This agent is not handling AI ATM. 
/*
	Empty base: Speciální případ

	začáteční score = 0 
	defender = ten co prijde driv
	attacker = ten druhej
	kdyz vyhraje defender, tak se to stejne flipne
*/

ACF_battleAgent = {
	params ["_base"];

	// Find out if there is any enemy inside borders
	private _basePos = getPos _base;
	private _baseSide = GVAR(_base,"side");
	private _battleEnded = false;
	private _score = 100;
	private _winner = sideUnknown;
	private _emptyBase = false;
	private _flagPole = GVAR(_base,"flag");
	if (_baseSide == sideUnknown || _baseSide == sideEmpty) then {
		//systemChat "Battle for empty base";
		_emptyBase = true;
		_baseSide = GVAR(AC_battalions#0,"side");
		_score = 0;
	    [_flagPole, 0, true] call BIS_fnc_animateFlag;
	};
	private _enemySide = [_baseSide] call ACF_enemySide;
	private _flagDefender = GVAR([_baseSide] call ACF_battalion,"flag");
	private _flagAttacker = GVAR([_enemySide] call ACF_battalion,"flag");

	SVARG(_base,"contested",true);
	SEND_NOTIFICATION(NN_BASE_ATTACKED,GVAR(_base,"callsign"),GVAR(_base,"side"));

	while {!_battleEnded} do {
		sleep 1;
		private _nearSoldiers = (_base nearEntities [["Man","LandVehicle"],GVAR(_base,"out_perimeter")]) inAreaArray ([_basePos] + GVAR(_base,"objectArea"));
		{{_nearSoldiers pushBackUnique _x} forEach crew _x} forEach _nearSoldiers;
		private _nDefenders = {side _x == _baseSide} count _nearSoldiers;
		private _nAttackers = {side _x == _enemySide} count _nearSoldiers;

		private _change = (_nDefenders - _nAttackers) * BA_CHANGE_RATE;
		CLAMP(_change,-2,2);
		_score = _score + _change;
		CLAMP(_score,-100,100);
		SVARG(_base,"score",round _score);

		// Handle flag position and texture
	    [_flagPole, abs (_score / 100), false] call BIS_fnc_animateFlag;

		if (_score < 0 && getForcedFlagTexture _flagPole  != _flagAttacker) then {
			_flagPole forceFlagTexture _flagAttacker;
		};
		if (_score > 0 && getForcedFlagTexture _flagPole  != _flagDefender) then {
			_flagPole forceFlagTexture _flagDefender;
		};

		if (_score == -100 || {_nAttackers == 0 && _score == 100} || {_emptyBase && count _nearSoldiers == 0 && _score == 0}) then {
			_battleEnded = true;
		};
	};

	if (_score == -100) then {
		// Flip the base
		// TODO: Set up correct number of defenders now
		[_base, _enemySide, DEFAULT_COUNT_GARRISON] spawn AC_gar_changeOwner;
		[_base, _enemySide] call ACF_ai_battleEnded;
	};
	if (_score == 100) then {
		if (_emptyBase) then {
			[_base, _baseSide, DEFAULT_COUNT_GARRISON] spawn AC_gar_changeOwner;
			[_base, _baseSide] call ACF_ai_battleEnded;
		} else {
			// Base defended!
		};
	};
	//systemChat format ["Battle of %1 ended!",GVAR(_base,"callsign")];

	// Reset battle variables
	SVAR(_base,"bat_groups",[]);
	SVAR(_base,"state",BATTLE_STATE_ENDED);

	sleep 5;
	SVARG(_base,"contested",false);
};



