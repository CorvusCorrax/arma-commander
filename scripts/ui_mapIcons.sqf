#include "\AC\defines\commonDefines.inc"
#include "\AC\defines\markerDefines.inc"
// ---------------------------
// INIT MAP ICONS
// ---------------------------

// Determine player's color

ACF_initMapIcons = {
	// Show all group icons
	setGroupIconsVisible [true,false];
	setGroupIconsSelectable true;

	{[_x] call ACF_ui_createGroupIcon} forEach AC_operationGroups;
	{[group _x] call ACF_ui_createGroupIcon} forEach AC_bases;

	UGVAR("#map") ctrlAddEventHandler ["Draw", {
		params ["_map"];

		[_map] call ACF_ui_drawMapIcons;
		[_map] call ACF_ui_drawSelectionBox;
		if (IS_COMMANDER) then {
			[] call ACF_ui_drawCursorIcon;
		};
	}];
};

ACF_ui_drawMapIcons = {
	params ["_map"];

	// Prevent existence of null groups
	AC_operationGroups = AC_operationGroups - [grpNull];
	private _textSize = 0.06;

	private _drawVehicles = {
		{
			private _iconType = _x getVariable ["marker",""];
			if (_iconType == "") then {
				_iconType = getText (configfile >> "CfgVehicles" >> typeOf _x >> "Icon");
				_x setVariable ["marker", _iconType];
			};
			private _pos = getPosWorld _x;
			_map drawIcon [ICON_CIRCLE_FILL, COLOR_VEHICLE, _pos, SIZE_GROUP_CIRCLE, SIZE_GROUP_CIRCLE, 0, "", 0, 0.03, 'TahomaB', 'right'];
			_map drawIcon [_iconType, COLOR_CONTRAST_VEHICLE, _pos, SIZE_ICON, SIZE_ICON, getDir _x, "", 0, 0.03, 'TahomaB', 'right'];
		} forEach (_this select 0);
	};

	private _drawBases = {
		params ["_units",["_selected",false]];
		private _borderColor = COLOR_CONTRAST_VEHICLE;
		if (_selected) then {_borderColor = COLOR_SELECTED_GROUP};

		{
			private _colorIcon = COLOR_BASE_CONTRAST;
			if (GVAR(_x,"contested")) then {_colorIcon = [1,0,0,1]};
			private _colorSide = [_x getVariable "side"] call BIS_fnc_sideColor;
			_colorSide set [3, 0.8];
			private _callsign = GVAR(_x,"callsign");
			private _pos = getPosWorld _x;

			_map drawIcon [ICON_BASE_FILL,_colorSide,_pos,80,40,0,_callsign,2,_textSize,'TahomaB','right'];
			_map drawIcon [ICON_BASE_OUTLINE,_borderColor,_pos,80,40,0,"",0,0.03,'TahomaB','right'];
			_map drawIcon [ICON_BASE,_colorIcon,_pos,SIZE_ICON,SIZE_ICON,0,"",0,0.03,'TahomaB','right'];
		} forEach (_this#0);
	};

	private _drawWaypoints = {
		{
			private _waypoint = getWPPos [_x, currentWaypoint _x];
			//private _waypoint = _x getVariable ["#wp",[0,0,0]];
			//private _rgba = [side _x] call BIS_fnc_sideColor;
			private _rgba =  [0.8,0.8,0,1]; // Yellow
			if !(_x getVariable ["canGetOrders",true]) then {_rgba = [1,1,1,1]};

			if (_waypoint isEqualType [] && {_waypoint#0 != 0} || (_waypoint isEqualType objNull && {!isNull _waypoint})) then {
				_map drawLine [getPosWorld leader _x, _waypoint, _rgba];
				_map drawIcon [ICON_WAYPOINT, _rgba, _waypoint, 20, 20, 0, "", 0, 0.03, 'TahomaB', 'right'];
			};
		} forEach ((_this#0) select {{alive _x} count units _x > 0});
	};

	private _drawGroups = {
		params ["_units",["_selected",false]];
		private _borderColor = COLOR_CONTRAST_VEHICLE;
		private _friendlyPlayerGroups = [];
		{_friendlyPlayerGroups pushBackUnique (group _x)} forEach (allPlayers select {side group _x == AC_playerSide});
		if (_selected) then {_borderColor = COLOR_SELECTED_GROUP};
		{
			private _colorIcon = COLOR_BASE_CONTRAST;
			if (_x == group player) then {
				_colorIcon = [0,1,0,1];
			} else {
				if (side _x == AC_playerSide && {_x in _friendlyPlayerGroups}) then {
					_colorIcon = [1,1,0,1];
				};
			};

			private _iconType = GVARS(_x,"marker","");
			private _colorSide = [side _x] call BIS_fnc_sideColor;
			_colorSide set [3,0.8];
			private _callsign = GVARS(_x,"callsign","");
			private _pos = getPosWorld leader _x;

			_map drawIcon [ICON_CIRCLE_FILL, _colorSide, _pos, SIZE_GROUP_CIRCLE, SIZE_GROUP_CIRCLE, 0, _callsign,2,_textSize, 'TahomaB', 'right'];
			_map drawIcon [ICON_CIRCLE_BORDER,	_borderColor, _pos, SIZE_GROUP_CIRCLE, SIZE_GROUP_CIRCLE, 0, "", 0, 0.03, 'TahomaB', 'right'];
			_map drawIcon [_iconType, _colorIcon, _pos, SIZE_ICON, SIZE_ICON, 0, "", 0, 0.03, 'TahomaB', 'right'];
		} forEach (_this#0);
	};

	/*
		Order of draw:
		- empty vehicles
		- bases
		- waypoints
		- groups
		- selected entities and their properties
	*/

	private _vehiclesToDraw = entities [["LandVehicle","Helicopter_Base_F","Plane","Ship_F"], [], false, true] select {{alive _x} count crew _x == 0};
	[_vehiclesToDraw] call _drawVehicles;

	private _basesToDraw = AC_bases - AC_selectedGroups;
	if (!DEBUG_MODE) then {
		_basesToDraw = _basesToDraw select {GVAR(_x,"detected") || {GVAR(_x,"side") == AC_playerSide}};
	};
	[_basesToDraw] call _drawBases;

	private _waypoints = (AC_operationGroups select {side _x == AC_playerSide}) - AC_selectedGroups;
	if (!isNull AC_mouseOver && {side _x == AC_playerSide} && {AC_selectedGroups find AC_mouseOver == -1}) then {
		_waypoints pushBack AC_mouseOver;
	};
	if (DEBUG_MODE) then {_waypoints = AC_operationGroups};
	[_waypoints] call _drawWaypoints;

	private _groups = (AC_operationGroups - AC_selectedGroups) select {(side _x == AC_playerSide || {_x getVariable ["detected",false]}) && {!([_x] call ACF_isTransported)}};
	if (DEBUG_MODE) then {_groups = AC_operationGroups};
	[_groups] call _drawGroups;

	// Selected groups + WP will be drawn last, so it is not covered by anyone
	// TODO: Different Alpha
	private _selectedGroups = AC_selectedGroups select {!([_x] call ACF_isTransported)};
	if (count _selectedGroups == 0) exitWith {};

	private _firstSelected = _selectedGroups select 0;

	if (!isNull GVARS(_firstSelected,"base",objNull)) then {
		[[leader _firstSelected],true] call _drawBases;
	} else {
		[_selectedGroups select {side _x == AC_playerSide},true] call _drawWaypoints;
		[_selectedGroups,true] call _drawGroups;
	};
};

// ----------------------
// GROUP ICONS MANAGEMENT
// ----------------------

// Group icon contains only text, no longer dynamic icon
ACF_ui_createGroupIcon = {
	params ["_group"];
	if (isNull _group) exitWith {};
	//exit if group icon exists
	if !(((getGroupIcons _group) select 0) isEqualTo [1,1,1,1]) exitWith {
		debugLog "Group icon already existst!";
	};

	private _side = side _group;
	if (!isNull GVARS(_group,"base",objNull)) then {
		_side = (leader _group) getVariable "side";
	};

	private _visible = true;
	if (_side != AC_playerSide) then {
		_visible = _group getVariable ["detected", true];
	};

	// Group icon contains only dummy icon, real icon is not group icon
	_group addGroupIcon ["Dummy",[0,0]];
	_group setGroupIconParams [AC_playerSideColor,"",1,_visible];
	//debugLog str ["Created group icon",GVAR(_group,"callsign"),_group];
};

// Local reveal function
ACF_ui_revealGroup = {
	params ["_entity"];
	private _group = grpNull;
	if (_entity isEqualType objNull) then {
		_group = group _entity;
		SEND_NOTIFICATION(NN_BASE_DETECTED,"",player);
	} else {
		SEND_NOTIFICATION(NN_GROUP_DETECTED,"",player);
		_group = _entity;
	};

	private _params = getGroupIconParams _group;
	_params set [3,true];
	_group setGroupIconParams _params;
};

ACF_ui_forgetGroup = {
	params ["_entity"];
	private _group = grpNull;
	if (_entity isEqualType objNull) then {
		_group = group _entity;
	} else {
		_group = _entity;
	};

	private _params = getGroupIconParams _group;
	_params set [3,false];
	_group setGroupIconParams _params;
};

ACF_ui_drawSelectionBox = {
	params ["_map"];

	if (count AC_mouseDownPos == 0 || !visibleMap) exitWith {AC_mouseDownPos = []};
	if (getMousePosition distance2D AC_mouseDownPos < 0.05) exitWith {};

	private _cursorCurPos = _map ctrlMapScreenToWorld getMousePosition;
	private _originalCurPos = _map ctrlMapScreenToWorld AC_mouseDownPos;
	_originalCurPos params ["_origX","_origY"];
	_cursorCurPos params ["_curX","_curY"];

	// Draw rectangle
	private _middlePos = [_originalCurPos, _cursorCurPos] call ACF_middlePos;
	private _lenX = (abs _origX - _curX) / 2;
	private _lenY = (abs _origY - _curY) / 2;

	_map drawRectangle [_middlePos, _lenX, _lenY, 0,COLOR_SELECTION_BOX,""];

	// Put correct groups into selection
	AC_selectedGroups = [];
	{
		(getPosWorld leader _x) params ["_leaderX","_leaderY"];
		private _limitsX = [_origX, _curX];
		private _limitsY = [_origY, _curY];
		{_x sort true} forEach [_limitsX, _limitsY]; 

		if (side _x == AC_playerSide && 
			{_leaderX >= _limitsX#0} &&
			{_leaderX <= _limitsX#1} &&
			{_leaderY >= _limitsY#0} &&
			{_leaderY <= _limitsY#1}
		) then {
			AC_selectedGroups pushBack _x;
		};
	} forEach AC_operationGroups;
};