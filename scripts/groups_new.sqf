#include "\AC\defines\commonDefines.inc"

AC_canSpawn = true;

ACF_grp_createGroupPara = {
	params ["_type","_side","_pos"];

	// UnitList, vehicleList, skill
	private _soldiers = [_type,"units"] call ACF_getGroupArray;
	private _vehicles = [_type,"vehicles"] call ACF_getGroupArray;
	private _skill = [_type,_side] call ACF_getSkill;

	private _group = createGroup _side;
	_group deleteGroupWhenEmpty true;
	_group setCombatMode "BLUE";

	private _callsign = [_group] call ACF_grp_setCallsign;
	private _groupType = [_type,"type"] call ACF_getGroupNumber;
	private _icon = [_type,"icon"] call ACF_getGroupString;
	if (_icon == "") then {
		_icon = [_type,"marker"] call ACF_getGroupString;
	};

	// Create all units
	private _handles = [];
	private _vehicleObjects = [];
	_pos = [_pos] call ACF_findLandingPos;
	_pos set [2,100];

	waitUntil {AC_canSpawn};
	{
		sleep 1;

		// Create vehicle and his parachute, give him proper position
		private _vehicle = createVehicle [_x,_pos,[],50,"FLY"];
		_vehicle allowDamage false;
		private _para = "B_Parachute_02_F" createVehicle [0,0,0];
		private _finalPos = getPosWorld _vehicle;
		_finalPos set [2,120];
		_para setPos _finalPos;
		_vehicle attachTo [_para,[0,0,0]];
		_vehicleObjects pushBack _vehicle;
		_vehicle limitSpeed VEHICLE_SPEED_LIMIT;

		private _h = [_vehicle,_para,true] spawn ACF_handleParadrop;
		_handles pushBack _h;
	} forEach _vehicles;

	{
		sleep 1;
		private _unit = _group createUnit [_x,_pos,[],50,"FLY"];
		_unit allowDamage false;
		_unit disableAI "all";
		_unit setSkill _skill;

		private _inVehicle = [_unit,_vehicleObjects] call ACF_grp_moveInFreeVehicle;

		if (isNull _inVehicle) then {
			private _para = "Steerable_Parachute_F" createVehicle [0,0,0];
			private _finalPos = getPosWorld _unit;
			_finalPos set [2,120];
			_para setPos _finalPos;
			_unit moveInDriver _para;

			private _h = [_unit,_para,false] spawn ACF_handleParadrop;
			_handles pushBack _h;
		};
	} forEach _soldiers;
	AC_canSpawn = true;

	private _data = [_type,_callsign,_skill];
	SEND_GROUP_DATA(_group,_data);
	[_group] call ACF_grp_saveGroupLoadouts;
	if (count _vehicleObjects > 0) then {
		_group selectLeader (effectiveCommander (_vehicleObjects#0));
	};

	waitUntil {_handles findIf {!(scriptDone _x)} == -1};
	_group setCombatMode "YELLOW";
	{_x enableAI "all"} forEach units _group;

	sleep 10;
	{_x allowDamage true} forEach (units _group + _vehicleObjects);
};

ACF_grp_saveGroupLoadouts = {
	params ["_group"];
	private _side = side _group;
	private _battalion = [_side] call ACF_battalion;
	private _loadouts = GVARS(_battalion,"loadouts",[]);
	private _nLoadouts = count _loadouts;
	{
		private _type = typeOf _x;
		if (_loadouts findIf {_x#0 == _type} == -1) then {
			_loadouts pushBack [_type, getUnitLoadout _x];
		};
	} forEach units _group;
	if (count _loadouts != _nLoadouts) then {
		SVAR(_battalion,"loadouts",_loadouts);
	};
};

ACF_handleParadrop = {
	params ["_unit","_para","_isCar"];
	sleep 5;
	private _t = time + 45;
	waitUntil {(getPos _unit)#2 < 1 || {time > _t}};

	if (_isCar) then {
		detach _unit;
		_unit SetVelocity [0,0,-5];
		sleep 0.3;
		_unit SetVelocity [0,0,0];
	};

	private _landingPos = getPosATL _unit;
	_landingPos set [2,0];
	_unit setVehiclePosition [getPos _unit, [],0,"FORM"];
	_unit enableAI "all";
};

ACF_getSkill = {
	params ["_type","_side"];
	private _module = MGVARS(_type,objNull);
	private _skill = 0.4;
	if (isNull _module) then {
		private _battalion = [_side] call ACF_battalion;
		private _batConfig = configfile >> "AC" >> "Battalions" >> GVAR(_battalion,"type");
		_skill = getNumber (_batConfig >> "combatElement" >> _type >> "skill");
	} else {
		_skill = GVAR(_module,"skill");
	};
	_skill
};

ACF_getGroupVehicles = {
	params ["_group"];
	private _vehicles = (units _group) select {private _veh = objectParent _x; 
		!isNull _veh && {_x == effectiveCommander _veh}
		&& {typeOf _veh != "Steerable_Parachute_F" && typeOf _veh != "B_Parachute_02_F"}
	};
	_vehicles = _vehicles apply {objectParent _x};
	_vehicles
};