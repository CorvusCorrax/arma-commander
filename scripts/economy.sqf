#include "\AC\defines\commonDefines.inc"

/*
	Economy functions:
	- Adding resources to battalions
	- AI: Buying new units
*/

// Find out rate of recieving RP:
AC_economyTick = GVARS(AC_gameModule,"IncomeRate",60);

// Set income params should be set up very early, before player gets into game
ACF_ec_setIncomeParams = {
	// Check starting resources
	private _resources = ["StartingResources",-1] call BIS_fnc_getParamValue;
	if (_resources > -1) then {
		{
			SVARG(_x,"points",_resources);
		} forEach AC_battalions;
	};

	// Check economy tick rate
	private _rate = ["IncomeRate",-1] call BIS_fnc_getParamValue;
	if (_rate > -1) then {
		AC_economyTick = _rate;
	};

	// Sort out AI difficulty
	AC_startingRpRatioAI = 1;
	AC_incomeRatioAI = 1;
	private _difficulty = ["AiDifficulty",1] call BIS_fnc_getParamValue;
	switch (_difficulty) do {
		case 1: {
			AC_startingRpRatioAI = 1.5;
			AC_incomeRatioAI = 1.5;
		};
		case 2: {
			AC_startingRpRatioAI = 2;
			AC_incomeRatioAI = 2;
		};
		case 3: {
			AC_startingRpRatioAI = 3;
			AC_incomeRatioAI = 2.5;
		};
	};
};

ACF_ec_economyAgent = {
	{
		[_x] spawn ACF_buyStartingComposition;
	} forEach (AC_battalions select {IS_AI_ENABLED(_x)});

	private _basicRate = GVAR(AC_gameModule,"IncomeMultiplier");
	MSVARG("AC_nextIncomeTime",time + AC_economyTick);
	while {!AC_ended} do {
		sleep AC_economyTick;
		{
			// Calculate how many points are added
			private _side = GVAR(_x,"side");
			private _basesCount = count (AC_bases select {GVAR(_x,"side") == _side});

			// Add extra income to AI according to difficulty
			if (IS_AI_ENABLED(_x)) then {
				_basesCount = _basesCount * AC_incomeRatioAI;
			};

			// Execute for all clients
			[_x,_basesCount * _basicRate] remoteExecCall ["ACF_ec_addRp",0,true];

			if (IS_AI_ENABLED(_x)) then {
				[_x] call ACF_ec_checkRequisition;
				[_x] spawn ACF_ec_checkForReinforcements;
			};
		} forEach AC_battalions;
	};
};

AC_capPlayer = 1000;
AC_capAI = 1000;
#define POINTS_CAP 1000

// Locally adding requisition points - easier to update UI alongside
ACF_ec_addRp = {
	params ["_battalion", "_added"];
	private _points = GVAR(_battalion,"points") + _added;
	_points = _points min POINTS_CAP;
	SVAR(_battalion,"points",_points);
	AC_nextIncomeTime = time + AC_economyTick;

	if (hasInterface && {visibleMap} && {GVAR(_battalion,"side") == side group player}) then {
		[] call ACF_ui_updateBattalionInfo;
		[] call ACF_ui_updateBuyList;
		playSound "Beep_Target";
	};
};

ACF_ec_unitTable = {
	params ["_battalion"];
	private _table = GVARS(_battalion,"unitTable",[]);

	if (count _table == 0) then {
		{
			private _count = ceil ((getNumber (_x >> "count")) * AC_unitCapRatio);
			_table pushBack [configName _x, _count,getNumber (_x >> "cost"),_count];
		} forEach ("true" configClasses (configfile >> "AC" >> "Battalions" >> GVAR(_battalion,"type") >> "combatElement"));

		// Write unit table entries into the table itself
		private _side = GVAR(_battalion,"side");
		{
			if (_side == [GVAR(_x,"side")] call AC_fnc_numberToSide) then {
				private _count = ceil (GVAR(_x,"count") * AC_unitCapRatio);
				_table pushBack [GVAR(_x,"typeStr"), _count, GVAR(_x,"cost"),_count];
			};
		} forEach (entities "AC_ModuleRegisterGroup");

		SVARG(_battalion,"unitTable",_table);
	};
	_table // format [classname,count,cost];
};

// Find out if you can buy your next unit. If queue is empty, then buy unit that is most needed. 
// @ Possible improvement: According to current threat, find out which type of unit is needed to buy
// This is AI function!
ACF_ec_checkRequisition = {
	params ["_battalion"];
	private _reinforcementsQueue = GVARS(_battalion,"reinforcementsQueue",[]);
	private _availableRP = GVAR(_battalion,"points");
	private _unitTable = [_battalion] call ACF_ec_unitTable;

	private _nDeployedGroups = {!isNull _x} count (([_battalion] call ACF_combatGroups) + GVARS(_battalion,"fireSupport",[]));
	if (_nDeployedGroups >= AC_unitCap) exitWith {};

	// Reinforce or buy random unit according to counts that should be there:
	if (count _reinforcementsQueue > 0) then {
		private _tableIndex = _unitTable findIf {_x#0 == _reinforcementsQueue#0};

		if (_tableIndex == -1) exitWith {};
		(_unitTable#_tableIndex) params ["_unit","_count","_cost"];

		if (_count > 0 || {_count > 0 && _availableRP >= _cost}) then {
			_reinforcementsQueue deleteAt 0;
			SVAR(_battalion,"reinforcementsQueue",_reinforcementsQueue);
		};
		if (_count == 0) exitWith {}; // Unit cannot be bought

		[_battalion,_tableIndex] call ACF_ec_orderGroup;
	} else {
		private _units = [];
		private _weights = [];
		{
			_x params ["_unit","_count","_cost"];
			if (_availableRP >= _cost && _count > 0) then {
				_units pushBack _unit;
				//_weights pushBack _count;
			};
		} forEach _unitTable;

		// Weighted random should not be necessary now
		//private _unit = _units selectRandomWeighted _weights;
		private _unit = selectRandom _units;

		if (isNil "_unit") exitWith {};
		private _i = _units find _unit;
		if (_i > -1) then {
			[_battalion,_i] call ACF_ec_orderGroup;
		};
	};
};

ACF_ec_checkForReinforcements = {
	params ["_battalion"];
	// Reinforce units with some randomization
	{
		if (random 4 <= 1 && {[_x,_battalion] call ACF_canResupply > -1}) then {
			[_x, _battalion, 0] call ACF_resupplyGroup;
			sleep 5;
		};
	} forEach (([_battalion] call ACF_combatGroups) + GVAR(_battalion,"fireSupport"));
};

ACF_ec_orderGroup = {
	params ["_battalion","_tableIndex",["_pos",[]]];
	if (count _pos == 0) then {
		_pos = [_battalion] call ACF_ec_findBestSpawnPos;
	};
	private _table = [_battalion] call ACF_ec_unitTable;

	// Remove 1 count of unit from the list
	(_table#_tableIndex) params ["_unit","_count","_cost"];
	[_battalion, _unit,_pos] call ACF_ec_buyGroup;

	//systemChat str [_tableIndex, (_table#_tableIndex)];
	(_table#_tableIndex) set [1, _count - 1];
	SVARG(_battalion,"unitTable",+_table);
	SVARG(_battalion,"points", GVAR(_battalion,"points") - _cost);
	SVARG(_battalion,"nActiveGroups", GVAR(_battalion,"nActiveGroups") + 1);
};

ACF_ec_findBestSpawnPos = {
	params ["_battalion"];
	private _side = GVAR(_battalion,"side");
	private _attackedBase = objNull;
	private _spawnBase = objNull;
	private _pos = [];

	// Find best position to deploy unit
	private _basesToSpawn = AC_bases select {GVAR(_x,"side") == _side};
	private _borderBases = [_side] call ACF_borderBases;

	// Have only bases that are neighboring with border bases, but are not on border themselves
	_basesToSpawn = _basesToSpawn select {
		 !(_x in _borderBases)
		&& {GVAR(_x,"neighbors") findIf {_x in _borderBases} > -1}
	};

	if (count _basesToSpawn == 0) then {
		_basesToSpawn = AC_bases select {GVAR(_x,"side") == _side};
	};

	if (count GVARS(_battalion,"attacks",[]) > 0) then {
		private _attacks = [GVARS(_battalion,"attacks",[]),[],{GVARS(_x,"adRatio",100)},"ASCEND"] call BIS_fnc_sortBy;
		if (count _attacks > 0) then {_attackedBase = _attacks#0};

		if (!isNull _attackedBase) then {
			_basesToSpawn = [_basesToSpawn,[],{_x distance _attackedBase},"ASCEND"] call BIS_fnc_sortBy;
			if (count _basesToSpawn > 0) then {
				_spawnBase = _basesToSpawn#0;
				_pos = _spawnBase getRelPos [300, _spawnBase getRelDir _attackedBase];
			};
		};
	};

	// If there is no attack, just select random deployable base
	if (count _pos == 0 && {count _basesToSpawn > 0}) then {
		_spawnBase = selectRandom _basesToSpawn;
		_pos = getPos _spawnBase;
	};

	//systemChat format ["Deploying group at %1: %2",GVARS(_spawnBase,"callsign","Not base"),_pos];
	_pos
};

// This is most basic version, will neeed many improvements like
//	- Weighted random or ensuring balanced units are bought
// 	- Selecting optimal deployment position(s)
ACF_buyStartingComposition = {
	params ["_battalion"];
	// Buy combat units when you have time
	for "_i" from 0 to 20 do {
		if !(IS_AI_ENABLED(_battalion)) exitWith {};
		[_battalion] call ACF_ec_checkRequisition;
		sleep 10;
	};
};

ACF_ec_buyGroup = {
	params ["_battalion", "_groupToBuy",["_pos",[]]];
	if (count _pos == 0) then {
		_pos = getPosWorld _battalion;
	};
	private _radius = 50;
	if (IS_AI_ENABLED(_battalion)) then {_radius = 250};

	_pos = [_pos, _radius] call ACF_randomPos;
	_pos = [_pos, 1, 100, 6, 0, 20, 0] call BIS_fnc_findSafePos;

	[_groupToBuy, _battalion getVariable "side", _pos] spawn ACF_grp_createGroupPara;
};
