#include "\AC\defines\commonDefines.inc"

// Make sure sides are enemies
west setFriend [east,0];
west setFriend [resistance,0];
east setFriend [west,0];
east setFriend [resistance,0];
resistance setFriend [east,0];
resistance setFriend [west,0];
setWind [0, 0, true];

// Enable dynamic simulation to props
enableDynamicSimulationSystem true;
[] spawn {
	private _props = ["ReammoBox","HouseBase","ReammoBox_F"];
	{
		{
			_x enableDynamicSimulation true;
			uiSleep 0.01;
		} forEach allMissionObjects _x;
	} forEach _props;
};

addMissionEventHandler ["HandleDisconnect",{
	debugLog "HandleDisconnect";
	_this call ACF_com_handleDisconnect;
}];

MSVARG("AC_serverInitDone",true);

sleep 2;

[] call ACF_ec_setIncomeParams;

// Server time to end
AC_missionLength = GVAR(AC_gameModule,"Length");
private _paramLength = ["Length",-1] call BIS_fnc_getParamValue;
if (_paramLength > -1) then {
	AC_missionLength = _paramLength;
};

[] spawn {
	AC_endTimeServer = AC_missionLength - time;
	[AC_endTimeServer] remoteExec ["ACF_runEndTimer",0];
	while {true} do {
		sleep 0.1;
		AC_endTimeServer = (AC_missionLength - time) max 0;
	};
};

[] spawn {
	sleep 5;
	[] spawn ACF_frontlinesModeAgent;
};

// Init agents and main checks
[] spawn {
	sleep 10;
	private _t = time + 20;
	// All players are alive, or timeout ran out
	waitUntil {sleep 1;
		!isMultiplayer || {time > _t}
		|| {playersNumber west + playersNumber east + playersNumber resistance == {alive _x} count allPlayers}
	};

	[] spawn ACF_ai_strategyAgent;
	sleep 0.1;
	[] spawn ACF_ec_economyAgent;
};

while {true} do {
	sleep 0.1;
	[] call ACF_checkSimulation;
	sleep 0.1;
	[] call ACF_groupdetectionCheck;
	sleep 0.1;
	[] call ACF_baseDetectionCheck;
	sleep 0.1;
	[] call ACF_checkBattles;
	sleep 0.5;
};