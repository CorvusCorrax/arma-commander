#include "\AC\defines\commonDefines.inc"

/*
	Battalion synchronized variables:

*/

// Return battalion for your side
ACF_battalion = {
	params ["_side"];
	if (_side isEqualType 0) then {_side = [_side] call AC_fnc_numberToSide};
	private _i = (AC_battalions findIf {_x getVariable "side" isEqualTo _side});
	if (_i == -1) exitWith {objNull};
	AC_battalions#_i // This is return value
};

ACF_createCustomBattalion = {
	params ["_module"];
	// Init customBatModule - register reserves
	private _units = [];
	{
		{
			_units pushBack (typeOf _x);
			deleteVehicle _x;
		} forEach (units group _x); 
	} forEach ((synchronizedObjects _module) select {_x isKindOf "Man"});
	SVARG(_module,"reserves",_units);
};

ACF_initBattalion = {
	params ["_battalion"];
	private _side = [GVAR(_battalion,"side")] call AC_fnc_numberToSide;
	SVAR(_battalion,"side",_side);
	private _type = [_battalion, _side] call ACF_battalionType;

	// Register custom battalions
	private _customBatModule = objNull;
	if (_type == "Custom") then {
		private _customBattalions = entities "AC_ModuleCustomBattalion";
		private _i = _customBattalions findIf {[GVAR(_x,"side")] call AC_fnc_numberToSide == _side};
		if (_i > -1) then {
			_customBatModule = _customBattalions#_i;
		};
	};
	if (_type == "Custom" && isNull _customBatModule) exitWith {
		["Create Custom Battalion Module not found!"] call BIS_fnc_error;
	}; 

	// Write variables into object
	_battalion setVariable ["reinforcementsQueue",[]];
	_battalion setVariable ["attacks",[]];
	_battalion setVariable ["nActiveGroups",0];
	if (isNull _customBatModule) then {
		private _batConfig = configfile >> "AC" >> "Battalions" >> _type;
		_battalion setVariable ["faction",getText (_batConfig >> "faction")];
		_battalion setVariable ["flag",getText (_batConfig >> "flag")];
	} else {
		_battalion setVariable ["faction",GVAR(_customBatModule,"faction"),true];
		_battalion setVariable ["flag",GVAR(_customBatModule,"flag"),true];
		_battalion setVariable ["module",_customBatModule,true];
	};

	// Initialize list buy list (including tooltip). Format: Classname, cost, name, tooltip
	private _list = [];
	if (isNull _customBatModule) then {
		{
			private _grpConf = configfile >> "AC" >> "AC_groups" >> configName _x;
			_list pushBack [configName _x, getNumber (_x >> "cost"),getText (_grpConf >> "name"), getText (_grpConf >> "tooltip")];
		} forEach ("true" configClasses (configfile >> "AC" >> "Battalions" >> GVAR(_battalion,"type") >> "combatElement"));
	};

	// Find out if there are any custom groups to add
	{
		if (_side == [GVAR(_x,"side")] call AC_fnc_numberToSide) then {
			_list pushBack [vehicleVarName _x, GVAR(_x,"cost"),GVAR(_x,"name"), GVAR(_x,"tooltip")];
		};
	} forEach (entities "AC_ModuleRegisterGroup");

	SVAR(_battalion,"ec_unitList",_list);
};

// Check if battalion type was overriden
ACF_battalionType = {
	params ["_battalion","_side"];
	private _param = "BattalionInd";
	if (_side == west) then {_param ="BattalionWest"};
	if (_side == east) then {_param = "BattalionEast"};

	private _type = [_param,-1] call BIS_fnc_getParamValue;
	if (_type == -1) then {
		_type = GVAR(_battalion,"type");
	};

	switch (_type) do {
		case INFANTRY_O: 		{_type = "Infantry_O"};
		case TANK_O: 			{_type = "Tank_O"};
		case RECON_O: 			{_type = "Recon_O"};
		case MILITIA_FIA: 		{_type = "Militia_FIA"};
		case RECON_B:			{_type = "Recon_B"};
		case TANK_B: 			{_type = "Tank_B"};
		case MECHANIZED_I: 		{_type = "MECHANIZED_I"};
		case MILITIA_SYNDIKAT:	{_type = "Militia_Syndikat"};
		case CUSTOM:			{_type = "Custom"};
	};
	SVAR(_battalion,"type",_type);
	_type
};

// Register player group into battalion
ACF_initHqElement = {
	params ["_group","_battalion","_side"];
	if (_group in AC_operationGroups) exitWith {};

	private _type = "";
	switch (_side) do {
		case WEST: 			{_type = "B_hqSquad"};
		case EAST: 			{_type = "O_hqSquad"};
		case RESISTANCE:	{_type = "I_hqSquad"};
	};

	// Initialize group:
	private _data = [_type,"Command",0.4];
	SEND_GROUP_DATA(_group,_data);
	SVARG(_battalion,"hqElement",_group);
};

// Return all bases that are neighboring different side
ACF_borderBases = {
	params ["_side"];
	private _bases = [];
	private _sideBases = AC_bases select {GVAR(_x,"side") == _side};
	{
		if (GVARS(_x,"neighbors",[]) findIf {GVAR(_x,"side") != _side} > -1) then {
			_bases pushbackunique _x;
		};  
	} forEach _sideBases;

	// All non-side bases that are neighboring the side
	{
		if (GVARS(_x,"neighbors",[]) findIf {GVAR(_x,"side") == _side} > -1) then {
			_bases pushbackunique _x;
		};  
	} forEach (AC_bases select {GVAR(_x,"side") != _side});

	_bases
};

// Total strength of all COMBAT units (might get updated later)
// NOT USED ATM
ACF_ai_battalionStrength = {
	params ["_battalion"];

	// TODO: Do I want to count also artillery?
	private _allGroups = [_battalion] call ACF_combatGroups;
	private _totalStrength = 0;

	// Current combat effectiveness
	{
		_totalStrength = _totalStrength + ([_x] call ACF_ai_groupStrength);
	} forEach _allGroups;
	
	_battalion setVariable ["strength", _totalStrength];
	_totalStrength
};