#include "\AC\defines\commonDefines.inc";

/*
	Initialization of all bases: Script running on each machine locally
	Synchronized variables:
		- side
		- name
		- nSoldiers
		- nSoldiersOriginal
		- flag
*/

ACF_nav_initBases = {
	{
		// @@Is this needed?
		if (isNil {GVAR(_x,"detected")}) then {
			private _detected = true;
			if (GVAR(_x,"hidden") == 1) then {_detected = false};
			SVAR(_x,"detected",_detected);
		};

		private _side = [GVAR(_x,"side")] call AC_fnc_numberToSide;
		SVAR(_x,"side",_side);

		if (isNil {GVAR(_x,"attacked")}) then {SVAR(_x,"attacked",sideUnknown)};
		[_x] call ACF_nav_setBaseName;
		[_x] call ACF_nav_setPerimeters;

		if (isServer) then {
			private _newGroup = createGroup civilian;
			[_x] join _newGroup;
			SVARG(_newGroup,"base",_x);
			[_x] call ACF_nav_setGarrison;
			[_x, GVAR(_x,"nSoldiers")] spawn AC_gar_createGarrison;
			[_x,_side] call ACF_createFlag;
		};
	} forEach AC_bases;

	// Create neighbor connections
	{[_x] call ACF_nav_scanNeighbors} forEach AC_bases;
	{[_x] call ACF_nav_rescanMissingNeighbors} forEach AC_bases;
};

// TODO: Take any shape for the base
ACF_nav_setPerimeters = {
	params ["_base"];
	// Can be circle only, take the larger area
	private _perimeter = GVARS(_base,"objectArea",BASE_PERIMETER_MIN);
	_perimeter = (_perimeter#0) max (_perimeter#1);
	CLAMP(_perimeter,BASE_PERIMETER_MIN,BASE_PERIMETER_MAX);

	private _innerPerimeter = _perimeter / 3;
	CLAMP(_innerPerimeter, BASE_IN_PERIMETER_MIN, BASE_IN_PERIMETER_MAX);

	_base setVariable ["in_perimeter", _innerPerimeter];
	_base setVariable ["out_perimeter", _perimeter];
};

ACF_nav_setBaseName = {
	params ["_base"];
	private _name = GVAR(_base,"callsign");
	private _finalName = "";

	if (_name == "") then {
		// Autodetect name from names of locations around it
		private _locationName = text ((nearestLocations [getPosWorld _base,
		["nameCity","Airport","NameMarine","NameCityCapital","NameVillage","NameLocal"],
		500])#0);

		if (!isNil "_locationName") then {
			_finalName = _locationName;

			// Make sure first letter is uppercase
			_strArray = _finalName splitString "";
			private _upper = toUpper (_strArray#0);
			_strArray set [0,_upper];
			_finalName = _strArray joinString "";
		} else {
			// Choose name automatically
			_finalName = [GVAR(_base,"side")] call ACF_phoneticalWordAuto;
		};
	} else {
		_finalName = _name;
	};
	SVAR(_base,"callsign",_finalName);
};

// Scan units in base and set their params as data
ACF_nav_setGarrison = {
	params ["_base"];
	private _synchronizedUnits = synchronizedObjects _base select {_x isKindOf "man"};

	private _garrisonGroup = grpNull;
	if (count _synchronizedUnits > 0) then {
		_garrisonGroup = group (_synchronizedUnits select 0);
	};
	private _positions = [];
	private _directions = [];
	private _specialAtt = [];
	private _stances = [];
	{
		_positions pushBack (getPosATL _x);
		_directions pushBack (direction _x);
		_stances pushBack (unitPos _x);
		private _objParent = objectParent _x;
		if (!isNull _objParent) then {
			_specialAtt pushBack _objParent;
		} else {
			_specialAtt pushBack objNull;
		};
		deleteVehicle _x;
	} forEach units _garrisonGroup;
	deleteGroup _garrisonGroup;

	SVAR(_base,"gar_positions",_positions);
	SVAR(_base,"gar_directions",_directions);
	SVAR(_base,"gar_specialAtt",_specialAtt);
	SVAR(_base,"gar_stances",_stances);
};

// Create connections between bases
#define DIR_DIFF 50
ACF_nav_scanNeighbors = {
	params ["_base"];
	// Make sure there is at least 1 node
	private _range = 2000;
	private _nearbyNodes = (nearestObjects [_base, ["AC_ModuleACBase"], _range]) - [_base];;
	while {count _nearbyNodes < 4 && count AC_bases > 5} do {
		_range = _range + 500;
		_nearbyNodes = (nearestObjects [_base, ["AC_ModuleACBase"], _range]) - [_base];
	};

	private _confirmedNeighbors = [];
	// Compare angles with other selected nodes
	{
		private _currentNode = _x;
		private _nodeDir = _base getRelDir _currentNode;

		private _hasGoodAngle = true;
		{
			private _baseDir = _base getRelDir _x;
			private _dirDiff = (_nodeDir max _baseDir) - (_nodeDir min _baseDir);
			if (_dirDiff > 180) then {
				_dirDiff = abs (_dirDiff - 360);
			};

			if (_dirDiff < DIR_DIFF) exitWith {
				_hasGoodAngle = false;
			};
		} forEach _confirmedNeighbors;

		if (_hasGoodAngle) then {
			_confirmedNeighbors pushBack _currentNode;
			if (DEBUG_MODE) then {[_base, _currentNode] call ACF_createLine};
		};
		
		// If there are enough neighbors, escape the loop.
		if (count _confirmedNeighbors >= 4) exitWith {};
	} forEach _nearbyNodes;

	// Write neighbors into array
	SVAR(_base,"neighbors",_confirmedNeighbors);
};

// Propagate yourself as neighbor if others don't have you in their database
ACF_nav_rescanMissingNeighbors = {
	params ["_base"];
	{
		private _baseNeighbors = GVARS(_x,"neighbors",[]);
		if !(_base in _baseNeighbors) then {
			_baseNeighbors pushBackUnique _base;
			SVAR(_x,"neighbors", _baseNeighbors);
		};
	} forEach GVARS(_base,"neighbors",[]);
};

// Create and change flag in one funtion
ACF_createFlag = {
	params ["_base","_side"];
	private _flag = GVARS(_base,"flag",objNull);
	if (isNull _flag) then {
		_flag = createVehicle ["FlagPole_F", getPos _base, [], 0, "CAN_COLLIDE"];
		SVARG(_base,"flag",_flag);
	};
	private _battalion = [_side] call ACF_battalion;
	private _texture = GVARS(_battalion,"flag","");
	_flag forceFlagTexture _texture;
	_flag
};