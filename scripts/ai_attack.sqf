#include "\AC\defines\commonDefines.inc"

ACF_ai_assignAttacks = {
	params ["_battalion"];
	private _side = GVAR(_battalion,"side");

	private _enemySide = [_side] call ACF_enemySide;
	private _ongoingAttacks = GVARS(_battalion,"attacks",[]);

	private _groups = AC_operationGroups select {
		side _x == _side
		&& {GVARS(_x,"canGetOrders",true)}
		&& {GVAR(_x,"type") != TYPE_ARTILLERY}
	};

	private _borderBases = [_side] call ACF_borderBases;
	private _targetBases = _borderBases select {
		GVAR(_x,"side") == _enemySide
		&& {GVARS(_x,"attacked",sideUnknown) != _side}
	};

	// Attack empty base if there is any
	private _emptyBases = _borderBases select {
		GVAR(_x,"side") == sideEmpty
		&& {GVARS(_x,"attacked",sideUnknown) != _side}
	};

	// Attack empty base and end function
	if (count _emptyBases > 0 && count _groups > 0) exitWith {
		private _emptyBase = _emptyBases#0;
		private _closestGroups = [_groups,[],{leader _x distance2D _emptyBase},"ASCEND"] call BIS_fnc_sortBy;
		private _closestGroup = _closestGroups#0;
		_group = _groups - [_closestGroup];
		//systemChat str ["Attacking empty base", GVAR(_emptyBase,"callsign")];
		[_emptyBase, [_closestGroup],_side] call ACF_ai_createOffensive;
	};

	// Attack occupied base
	if (count _ongoingAttacks < 3 && count _targetBases > 0) then {
		private _targetBases = [_targetBases,[],{GVAR(_x,"att_cost") * ((_x distance2D _battalion) /1000) - (GVAR(_x,"thr_currentStr") * 1)},"ASCEND"] call BIS_fnc_sortBy;
		private _targetBase = _targetBases#0;
		_groups = [_groups,[],{leader _x distance2D _targetBase},"ASCEND"] call BIS_fnc_sortBy;

		// Send adequate strength to attack:
		private _strength = 0;
		private _requiredStrength = GVAR(_targetBase,"att_cost");

		{
			_strength = _strength + ([_x] call ACF_ai_groupStrength);

			// Create attack only if enough soldiers are available
			if (_strength >= _requiredStrength) exitWith {
				_groups resize (_forEachIndex + 1);
				//systemChat str ["Creating attack",_strength, _requiredStrength, _groups apply {GVAR(_x,"callsign")}];
				[_targetBase, _groups,_side] call ACF_ai_createOffensive;
				private _attacks = GVARS(_battalion,"attacks",[]);
				_attacks pushBack _targetBase;
				SVAR(_battalion,"attacks", _attacks);
				//systemChat str (_attacks apply {_x getVariable "callsign"});
			};

			// If there are not enough troops, fire artillery
			{
				if !(IS_AI_ENABLED(_battalion)) exitWith {};
				if (random 10 <= 1) then {
					private _pos = [getPosWorld _targetBase, GVAR(_targetBase,"out_perimeter") / 2] call ACF_randomPos;
					//systemChat ("Preparation artillery fire on: " + GVAR(_targetBase,"callsign"));
					[_x, _pos] call AC_ai_fireMission;
				};
				sleep 10;
			} forEach GVARS(_battalion,"fireSupport",[]);
		} forEach _groups;
		//systemChat str ["STR", _strength, _requiredStrength, count _groups];
	};
};

#define AS_STAGING 	1
#define AS_ATTACK 	2

// Info about offensive should be stored in base logic
ACF_ai_offensiveAgent = {
	params ["_base","_side","_attackGroups"];
	private _battalion = [_side] call ACF_battalion;
	private _stagingDistance = OFFENSIVE_STAGING_DISTANCE + GVAR(_base,"out_perimeter");
	private _baseAttackDistance =  _stagingDistance * 1.25 + 50;

	SVAR(_base,"attacked",_side);
	private _offensiveName = "Offensive " + GVAR(_base,"callsign") + ": ";
	//systemChat (_offensiveName + "Offensive started");

	// Unregister units from all attacks and them register them to different attack
	{[_x,_battalion] call ACF_unregisterAttackGroup} forEach _attackGroups;
	SVAR(_base,"attackGroups",_attackGroups);

	// 1. STAGING PHASE
	// Order all units to move to staging area
	SVAR(_base,"attackStage",AS_STAGING);
	{
		SVAR(_x,"canGetOrders",false);
		[_x, _base, B_TRANSPORT, nil, 50] call ACF_ai_moveToStaging;
	} forEach _attackGroups;
	sleep 10;
	if !(IS_AI_ENABLED(_battalion)) exitWith {};

	[_base,_battalion,_side] spawn ACF_ai_attackAssignmentManager;

	// Wait until initial timeout, or all units are ready to attack
	private _stagingTimeout = time + 300; // 5 minutes are basic timeout
	waitUntil {
		sleep STRATEGY_TICK;
		GVAR(_base,"attackGroups") findIf {leader _x distance _base > _baseAttackDistance} == -1 
		|| {time > _stagingTimeout}
	};
	if !(IS_AI_ENABLED(_battalion)) exitWith {};


	if (time > _stagingTimeout) then {
		//systemChat (_offensiveName + "Attacking after timeout");
	} else {
		//systemChat (_offensiveName + "Attacking: all units ready");
	};

	// 2. START ATTACK

	// Use offensive 
	private _artilleries = GVARS(_battalion,"fireSupport",[]);
	if (GVAR(_base,"def_currentStr") > 5) then {
		{
			if !(IS_AI_ENABLED(_battalion)) exitWith {};
			if (random 3 <= 1) then {
				private _pos = [getPosWorld _base, GVAR(_base,"out_perimeter") / 2] call ACF_randomPos;
				[_x, _pos] call AC_ai_fireMission;
				//systemChat (_offensiveName + "Fire mission called");
			};
			sleep 10;
		} forEach GVARS(_battalion,"fireSupport",[]);
	};

	sleep 10;
	if !(IS_AI_ENABLED(_battalion)) exitWith {};

	// Order all attacking units to engage
	{
		[_x, _base, B_COMBAT, false, 50] call ACF_ai_move;
	} forEach GVAR(_base,"attackGroups");
	SVAR(_base,"attackStage",AS_ATTACK);

	// Check ending conditions
	private _ended = false;
	while {!_ended} do {
		sleep STRATEGY_TICK;
		if !(IS_AI_ENABLED(_battalion)) exitWith {};

		private _currentAttackers = GVAR(_base,"attackGroups");
		private _attackersInArea = _currentAttackers select {leader _x distance _base <= _baseAttackDistance};
		private _attackerStrength = 0;
		{_attackerStrength = _attackerStrength + ([_x] call ACF_ai_groupStrength)} forEach _attackersInArea;
		private _attDefRatio = _attackerStrength / (GVAR(_base,"def_currentStr") max 0.1);
		SVAR(_base,"adRatio",_attDefRatio);
		//systemChat (_offensiveName + "A/D: " + str _attDefRatio);

		// Victory condition
		if (GVAR(_base,"side") == _side) then {
			_ended = true;
			//systemChat (_offensiveName + "Victory");
		} else {
			// Retreat condition
			if (_attDefRatio <= AD_RETREAT_TRESHOLD) then {
				{
					[_x, [_x] call ACF_rtbPos,B_TRANSPORT,true] call ACF_ai_move;
				} forEach _currentAttackers;
				_ended = true;
				//systemChat (_offensiveName + "Retreat");
			};
		};

		// If attack is not ending, make sure no group is stuck on one place
		{
			if (currentWaypoint _x > 0) then {
				private _wp = [_x, _base, B_COMBAT, nil, 20] call ACF_ai_move;
				//_wp setWaypointType "SAD";
				//systemChat format ["%1 replanning their attack waypoint",GVAR(_x,"callsign")];
			};
		} foreach _currentAttackers;
	};

	// 3. End the attack, cleanup everything
	{
		SVAR(_x,"canGetOrders",true);
	} forEach GVAR(_base,"attackGroups");
	SVAR(_base,"attacked",sideUnknown);
	SVAR(_battalion,"attacks",GVAR(_battalion,"attacks") - [_base]);
	SVAR(_base,"attackGroups",[]);
};

// Perform AI move to staging area
ACF_ai_moveToStaging = {
	params ["_group","_base","_moveType","_randomization"];
	private _leaderPos = getPosWorld leader _group;
	private _distance = GVAR(_base,"out_perimeter") + OFFENSIVE_STAGING_DISTANCE;
	private _stagingPoint = _base getRelPos [_distance,_base getRelDir _leaderPos];
	debugLog str _distance;

	[_x, _stagingPoint, B_TRANSPORT, nil, 50] call ACF_ai_move;
};

ACF_ai_attackAssignmentManager = {
	params ["_base","_battalion","_side"];

	while {GVARS(_base,"attacked",sideUnknown) == _side} do {
		private _idealAttackStr = (GVAR(_base,"def_currentStr") * ATTACK_DEFENSE_RATIO) max 8;
		private _maxAttackStr = 60 min ((_idealAttackStr * 1.5) max 20);
		private _currentAttackers = GVARS(_base,"attackGroups",[]);

		// Check for all units you could put into attack
		private _availableGroups = _currentAttackers + (([_battalion] call ACF_combatGroups) select {GVARS(_x,"canGetOrders",true)});
		_availableGroups = [_availableGroups,[],{leader _x distance2D _base},"ASCEND"] call BIS_fnc_sortBy;

		private _newAttackers = [];
		private _newStr = 0;

		// These should be hard-assigned units
		{
			SVAR(_x,"canGetOrders",false);
			_newAttackers pushBackUnique _x;
			_newStr = _newStr + ([_x] call ACF_ai_groupStrength);

			if (_currentAttackers find _x == -1) then {
				if (GVAR(_base,"attackStage") == AS_STAGING) then {
					[_x, _base, B_TRANSPORT, nil, 50] call ACF_ai_moveToStaging;
				} else {
					[_x, _base, B_COMBAT, nil, 10] call ACF_ai_move;
				};
			};
			if (_newStr >= _idealAttackStr) exitWith {};
		} forEach _availableGroups;

		{
			SVAR(_x,"canGetOrders",true);
		} forEach (_currentAttackers - _newAttackers);

		{[_x,_battalion] call ACF_unregisterAttackGroup} forEach _newAttackers;
		SVAR(_base,"attackGroups",_newAttackers);
		//systemChat str [GVAR(_base,"callsign"), GVAR(_base,"attackGroups") apply {GVAR(_x,"callsign")}];
		sleep STRATEGY_TICK;
	};
};

ACF_unregisterAttackGroup = {
	params ["_group","_battalion"];
	{
		private _groups = +GVARS(_x,"attackGroups",[]);
		_groups = _groups - [_group];
		SVAR(_x,"attackGroups",_groups);
	} forEach GVARS(_battalion,"attacks",[]);
};