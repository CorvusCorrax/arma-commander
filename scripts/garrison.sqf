#include "\AC\defines\commonDefines.inc"
/*
	All garrison functionality.

	Synchronized variables:
	- Detected
	- Side
	- nSoldiers
	- nSoldiersOriginal
*/

//give a garrison all data it needs: side, troops number, postions
AC_gar_createGarrison = {
	params ["_base", "_troopCount",["_visible",true]];
	private _side = _base getVariable "side";
	_base setVariable ["deployed", DEPLOYED_FALSE];
	(group _base) setVariable ["detected", _visible, true]; //visibility for enemy
	_base setVariable ["nSoldiersOriginal", _troopCount, true];
};

AC_gar_deployGarrison = {
	params ["_base"];
	private _side = _base getVariable "side";
	private _battalion = [_side] call ACF_battalion;

	if (_base getVariable "deployed" != DEPLOYED_FALSE || _side == sideEmpty || _side == sideunknown) exitWith {};
	_base setVariable ["deployed", DEPLOYED_BUSY];

	private _staticGroup = createGroup _side;
	private _dynamicGroup = createGroup _side;
	{_x enableDynamicSimulation true} forEach [_staticGroup,_dynamicGroup];

	_base setVariable ["staticGroup", _staticGroup];
	_base setVariable ["dynamicGroup", _dynamicGroup];
	_staticGroup setVariable ["base", _base];
	_dynamicGroup setVariable ["base", _base];

	private _positions = _base getVariable ["gar_positions",[]];
	private _directions = _base getVariable ["gar_directions",[]];
	private _stances = _base getVariable ["gar_stances",[]];
	private _specialPos = _base getVariable ["gar_specialAtt",[]];
	private _nSoldiers = _base getVariable ["nSoldiers",0];

	private _soldierArray = [];
	private _skill = 0.4;
	private _module = GVARS(_battalion,"module",objNull);
	if (isNull _module) then {
		private _conf = configfile >> "AC" >> "Battalions" >> _battalion getVariable "type" >> "Reserves";
		_soldierArray = getArray (_conf >> "units");
		_skill = getNumber (_conf >> "skill");
	} else {
		_soldierArray = GVAR(_module,"reserves");
		_skill = GVAR(_module,"skill");
	};

	// Decide static dynamic counts
	private _dynamicCount = ceil (_nSoldiers * GARRISON_STATIC_DYNAMIC_RATIO);
	private _staticCount = _nSoldiers - _dynamicCount;

	//make sure there are not more static units than static positions
	private _positionsCount = count _positions;
	if (_staticCount > _positionsCount) then {
		_staticCountOriginal = _staticCount;
		_staticCount = _positionsCount;
		_dynamicCount = _dynamicCount + (_staticCountOriginal - _staticCount);
	};

	//create static group in correct positions and turrets
	for "_i" from 0 to (_staticCount - 1) do {
		sleep SPAWN_TIMEOUT;
		//_unit = _staticGroup createUnit [selectRandom _soldierArray, _positions select _i , [], 0,"NONE"];
		private _unit = _staticGroup createUnit [selectRandom _soldierArray, [100,100,100], [], 0,"CAN_COLLIDE"];
		_unit setPosWorld _positions#_i;
		_unit setSkill _skill;

		//move soldier into turret or correct position and place
		if (!isNull (_specialPos select _i)) then {
			private _gun = _specialPos select _i;
			_unit assignAsGunner _gun;
			_unit moveInGunner _gun;
		} else {
			_unit setPos (_positions select _i);
			_unit setDir (_directions select _i);
			private _stance = _stances select _i;
			if (_stance != "UP") then {
				_unit setUnitPos _stance;
			};
		};
		_unit disableAI "PATH";

	    _staticGroup allowFleeing 0;
	    _dynamicGroup allowFleeing 0;
	};

	// Create dynamic group
	for "_i" from 1 to _dynamicCount do {
		sleep SPAWN_TIMEOUT;
		_unit = _staticGroup createUnit [selectRandom _soldierArray, getPos _base, [], 20,"FORM"];
		_unit setSkill _skill;
	};

	_base setVariable ["deployed", DEPLOYED_TRUE];

	//TODO: create patrols around the base (optional)
};

AC_gar_undeployGarrison = {
	params ["_base"];
	if (GVAR(_base,"deployed") != DEPLOYED_TRUE) exitWith {};
	_base setVariable ["deployed", DEPLOYED_BUSY];

	private _dynamicGroup = _base getVariable ["dynamicGroup",grpNull];
	private _staticGroup = _base getVariable ["staticGroup",grpNull];

	private _aliveSoldierCount = ({alive _x} count units _staticGroup) + ({alive _x} count units _dynamicGroup);
	//save current state of forces in base

	_base setVariable ["nSoldiers", _aliveSoldierCount, true];

	//delete all units + groups
	{
		{
			sleep DESPAWN_TIMEOUT;
			deleteVehicle _x;
		} forEach units _x;
		deleteGroup _x;
	} forEach [_dynamicGroup, _staticGroup];

	_base setVariable ["dynamicGroup", grpNull];
	_base setVariable ["staticGroup", grpNull];
	_base setVariable ["deployed", DEPLOYED_FALSE];
};

AC_gar_changeOwner = {
	// Change side, markers, soldier count
	params ["_base", "_side", "_soldierCount",["_notify",true]];
	
	_base setVariable ["side", _side, true];
	_base setVariable ["nSoldiers", 0, true];

	sleep 5;
	[_base,_side] call ACF_createFlag;

	if (_notify) then {
		private _texts = [_base getVariable "callsign", GVARS(([_side] call ACF_battalion),"faction", "unknown")];
		SEND_NOTIFICATION(NN_BASE_CAPTURED,_texts,0);
	};

	sleep 60;
	_base setVariable ["nSoldiers", _soldierCount, true];
	_base setVariable ["nSoldiersOriginal", _soldierCount, true];
	[_base, _soldierCount, true] call AC_gar_createGarrison;
};