#include "\AC\defines\commonDefines.inc"

/*
	Basic command UI events.
	Mostly if button was pressed, it will be handled by these functions:
*/

// Handle pressing switch button
ACF_ui_buttonSwitchPressed = {
	disableSerialization;
	params ["_group"];

	// Find unit to switch into:
	private _units = units _group select {alive _x};
	private _freeUnits = _units select {!isPlayer _x};
	private _unit = objNull;
	playSound "FD_CP_Clear_F";

	if (_freeUnits findIf {leader _group == _x} > -1) then {
		_unit = leader _group;
	} else {
		if (count _freeUnits > 0) then {
			_unit = _freeUnits#0;
		};
	};

	if (!isNull _unit) then {
		REQUEST_SWITCH(_unit);
	} else {
		// Disable switch button
		private _switchButton = (UGVAR("ac_groupInfo") ctHeaderControls 1) select 3;
		_switchButton ctrlEnable false;
	};
};

// Show or hide buy button
ACF_ui_buttonRequisitionPressed = {
	// Find out position of the button in the table
	private _battalion = AC_playerBattalion;
	private _index = (ctCurSel UGVAR("ac_buyList"));
	playSound "FD_CP_Clear_F";

	private _pos = [];
	if (AC_dropzone != "" && AC_markerPlaced) then {
		_pos = getMarkerPos AC_dropzone;
	} else {
		private _bases = [AC_playerSide] call ACF_findSpawnBases;
		_pos = getPos (selectRandom _bases);
	};

	private _nActiveGroups = GVAR(_battalion,"nActiveGroups");
	[_battalion, _index,_pos] remoteExec ["ACF_ec_orderGroup",2];

	// Wait for UI update
	_nActiveGroups spawn {
		waitUntil {GVAR(AC_playerBattalion,"nActiveGroups") != _this};
		[] call ACF_ui_updateCommandUI;
	};
};

ACF_ui_reinforceOrdered = {
	if (count AC_selectedGroups == 0 || {isNull (AC_selectedGroups#0)}) exitWith {};
	private _group = AC_selectedGroups#0;
	playSound "FD_CP_Clear_F";

	[_group,AC_playerBattalion,GVARS(_group,"resupplyCost",10)] remoteExec ["ACF_resupplyGroup",2];
};