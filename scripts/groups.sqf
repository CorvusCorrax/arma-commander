#include "\AC\defines\commonDefines.inc"

ACF_grp_moveInFreeVehicle = {
	params ["_unit","_vehicles"];
	if (count _vehicles == 0) exitWith {objNull};

	// Sort vehicles by amount of crew, descendant
	_vehicles = [_vehicles,[],{count crew _x},"ASCEND"] call BIS_fnc_sortBy;

	// Try to move vehicles inside
	private _in = objNull;
	{
		if (_unit moveInAny _x) exitWith {_in = _x};
	} forEach _vehicles;
	_in
};

AC_grp_handleKilledUnit = {
	params ["_killed"];
	private _group = group _killed;
	private _base = GVARS(_group,"base",objNull);
	private _side = side _group;

	debugLog "Handling killed unit";

	if (!isNULL _base) then {
		// Precise recounting of actual units alive
		private _nSoldiers = 0;
		{
			private _group = GVARS(_base,_x,grpNull);
			_nSoldiers = _nSoldiers + ({alive _x} count units _group);
		} forEach ["staticGroup","dynamicGroup"];
		SVARG(_base,"nSoldiers",_nSoldiers);
	};

	// Delete group if it's empty
	if ({alive _x} count units _group == 0) then {
		[_group,_side] spawn ACF_grp_handleEmptyGroup;
		_killed spawn {
			sleep 2;
			[_this] joinSilent grpNull;
		};
	} else {
		[_killed] joinSilent grpNull;
	};
};

ACF_grp_handleEmptyGroup = {
	params ["_group","_side"];
	private _callsign = GVAR(_group,"callsign");
	private _operationIndex = AC_operationGroups findIf {_group == _x};
	if (_operationIndex > - 1) then {

		// Delete group from operation group and battalion
		REMOVE_DATA_MISSION("AC_operationGroups",_group); // Remove group globally

		// Add group to queue for reinforcements
		private _battalion = [_side] call ACF_battalion;
		private _groupQueue = +(_battalion getVariable "reinforcementsQueue");
		_groupQueue pushBack (_group getVariable ["typeStr",""]);
		_battalion setVariable ["reinforcementsQueue", _groupQueue];
		SVARG(_battalion,"nActiveGroups", GVAR(_battalion,"nActiveGroups") - 1);

		// Delete other information
		private _otherSides = [WEST, EAST, RESISTANCE] - [_side];
		clearGroupIcons _group;
		sleep 5;

		SEND_NOTIFICATION(NN_GROUP_LOST,_callsign,_side);
		SEND_NOTIFICATION(NN_GROUP_KILLED,"",_otherSides);
	};

	// Let the group delete itself
	_group deleteGroupWhenEmpty true;
};

ACF_grp_setCallsign = {
	params ["_group", ["_callsign",""]];
	private _battalion = [side _group] call ACF_battalion;

	if (_callsign == "") then {
		_callsign = [_battalion] call ACF_assignCallsign;
	};
	_callsign
};

// Very simple function for now.
ACF_grp_addGroupXp = {
	params ["_killer", "_killed"];
	private _group = group _killer;
	private _nSoldiers = {alive _x} count units _group;
	private _skill = skill leader _group;

	// Friendly fire does not add any skill
	if (side group _killed == side group _killer || _nSoldiers == 0) exitWith {};

	private _newSkill = _skill + (XP_KILL_RATIO / _nSoldiers);
	if (_newSkill > 1) then {_newSkill = 1}; // Clamp

	// Change everyone's skill, and update group's skill
	{
		_x setSkill _newSkill;
	} forEach units _group;
};


// Co když jsou hráči součástí skupiny?
// Server function
ACF_resupplyGroup = {
	params ["_group","_battalion","_cost"];

	if (isNull _group) exitWith {};
	/*
		1. Find classes to create and spawn them above
		2. Change internal data, after everything is spawned
		3. Resupply all vehicles
	*/

	SVARG(_battalion,"points",GVAR(_battalion,"points") - _cost);

	private _newSoldiers = [GVAR(_group,"typeStr"),"units"] call ACF_getGroupArray;
	private _newVehicles = [GVAR(_group,"typeStr"),"vehicles"] call ACF_getGroupArray;

	private _skill = skill leader _group;
	private _soldiers = units _group select {alive _x};
	private _vehicles = [_group] call ACF_getGroupVehicles;
	private _currentSoldiers = _soldiers apply {typeOf _x};
	private _currentVehicles = _vehicles apply {typeOf _x};

	// Resupply all current soldiers and vehicles
	// Re-set loadouts for all units
	private _loadouts = GVARS(_battalion,"loadouts",[]);
	{
		private _type = typeOf _x;
		private _i = _loadouts findIf {_x#0 == _type};
		_x setUnitLoadout (_loadouts#_i#1);
	} forEach _soldiers;
	{_x setVehicleAmmoDef 1; _x setFuel 1} forEach _vehicles;
	{_x setDamage 0} forEach (_soldiers + _vehicles);

	// Remove those objects that already exist
	{
		private _str = _x;
		private _i = _newSoldiers findIf {_x == _str};
		_newSoldiers deleteAt _i;
	} forEach _currentSoldiers;
	{
		private _str = _x;
		private _i = _newVehicles findIf {_x == _str};
		_newVehicles deleteAt _i;
	} forEach _currentVehicles;

	// Spawn the rest on parachutes - should I use ACF_grp_handleParadrop?
	private _pos = getPosWorld leader _group;
	_pos set [2,200];
	private _handles = [];
	private _vehicleObjects = [];

	waitUntil {AC_canSpawn};
	{
		sleep 1;

		// Create vehicle and his parachute, give him proper position
		private _vehicle = createVehicle [_x,_pos,[],50,"FLY"];
		_vehicle allowDamage false;
		private _para = "B_Parachute_02_F" createVehicle [0,0,0];
		private _finalPos = getPosWorld _vehicle;
		_finalPos set [2,120];
		_para setPos _finalPos;
		_vehicle attachTo [_para,[0,0,0]];
		_vehicleObjects pushBack _vehicle;
		_vehicle limitSpeed VEHICLE_SPEED_LIMIT;

		private _h = [_vehicle,_para,true] spawn ACF_handleParadrop;
		_handles pushBack _h;
	} forEach _newVehicles;

	{
		sleep 1;
		private _unit = _group createUnit [_x,_pos,[],50,"FLY"];
		_unit allowDamage false;
		_unit disableAI "all";
		_unit setSkill _skill;

		private _inVehicle = [_unit,_vehicleObjects] call ACF_grp_moveInFreeVehicle;

		if (isNull _inVehicle) then {
			private _para = "Steerable_Parachute_F" createVehicle [0,0,0];
			private _finalPos = getPosWorld _unit;
			_finalPos set [2,120];
			_para setPos _finalPos;
			_unit moveInDriver _para;

			private _h = [_unit,_para,false] spawn ACF_handleParadrop;
			_handles pushBack _h;
		};
	} forEach _newSoldiers;
	AC_canSpawn = true;

	waitUntil {_handles findIf {!(scriptDone _x)} == -1};
	{_x enableAI "all"} forEach units _group;

	sleep 5;
	{
		_x allowDamage true;
	} forEach (units _group + _vehicleObjects);
};

ACF_canResupply = {
	params ["_group","_battalion"];

	// Find out if group is far away from any enemy base
	private _pos = getPos leader _group;
	private _enemySide = [side _group] call ACF_enemySide;
	private _i = (_pos nearEntities 600) findIf {side _x == _enemySide};
	if (_i > -1) exitWith {-1000};

	// Find out resupply cost, and save it to the group
	private _type = GVARS(_group,"typeStr","");
	private _unitData = GVARS(_battalion,"ec_unitList",[]);
	private _i = _unitData findIf {_type == _x#0};
	if (_i == -1) exitWith {-2000};

	private _defaultCost = _unitData#_i#1;
	private _cost = _defaultCost * (1 - (({alive _x} count units _group) / count ([_type,"units"] call ACF_getGroupArray)));

	private _newVehicle = false;
	if (_newVehicle) then {_cost = _cost + _defaultCost * 0.6};

	// Clamp between 25% and 100%
	_cost = round ((_cost min _defaultCost) max (_defaultCost * 0.25));

	if (GVARS(_group,"resupplyCost",-1) != _cost) then {
		SVAR(_group,"resupplyCost",_cost);
	};
	_cost
};
