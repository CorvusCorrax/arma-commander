#include "\AC\defines\commonDefines.inc"

saving = 0;
disabledAI = 1;
//enableDebugConsole = 2;

// Respawn settings
respawn = "INSTANT";
respawnTemplates[] = {};
respawnOnStart = 0;
respawnDialog = 0;
respawnDelay = 10e10;

// Revive settings
ReviveMode = 1;                         //0: disabled, 1: enabled, 2: controlled by player attributes
ReviveUnconsciousStateMode = 0;         //0: basic, 1: advanced, 2: realistic
ReviveRequiredTrait = 0;                //0: none, 1: medic trait is required
ReviveRequiredItems = 0;                //0: none, 1: medkit, 2: medkit or first aid kit
ReviveRequiredItemsFakConsumed = 0;     //0: first aid kit is not consumed upon revive, 1: first aid kit is consumed
ReviveDelay = 10;                       //time needed to revive someone (in secs)
ReviveMedicSpeedMultiplier = 2;         //speed multiplier for revive performed by medic
ReviveForceRespawnDelay = 3;            //time needed to perform force respawn (in secs)
ReviveBleedOutDelay = 120;              //unconscious state duration (in secs)

// Garbage collection
corpseManagerMode = 1;
corpseLimit = 10;
corpseRemovalMinTime = 30;
corpseRemovalMaxTime = 300;
wreckManagerMode = 1;
wreckLimit = 5;
wreckRemovalMinTime = 30;
wreckRemovalMaxTime = 300;
minPlayerDistance = 50;

// Params
class Params
{
	#include "\a3\functions_f\Params\paramDaytimeHour.hpp"
	#include "\a3\functions_f\Params\paramWeather.hpp"

	class BattalionWest
	{
		title = "Battalion West";
		values[] = {-1,MILITIA_FIA, RECON_B, TANK_B};
		texts[] = {"Default","FIA Battalion","NATO Ranger Battalion","NATO Mechanized Battalion"};
		default = -1;
 	};
	class BattalionEast
	{
		title = "Battalion East";
		values[] = {-1,INFANTRY_O,RECON_O,TANK_O};
		texts[] = {"Default","CSAT Guard Infantry Battalion","CSAT Recon Battalion","CSAT Tank Battalion"};
		default = -1;
 	};
	class BattalionInd
	{
		title = "Battalion Independent";
		values[] = {-1, MECHANIZED_I, MILITIA_SYNDIKAT};
		texts[] = {"Default","AAF Mechanized Battalion","Syndikat Militia Battalion"};
		default = -1;
 	};
	class Length
	{
		title = "Scenario Length";
		values[] = {-1,900,1200,1800,2700,3600,5400,7200,36000};
		texts[] = {"Default","15 Minutes","20 Minutes","30 Minutes","45 Minutes","1 Hour","1.5 Hours","2 Hours","10 Hours"};
		default = -1;
	};
	class UnitCap
	{
		title = "Maximum Groups per Side";
		values[] = {-1,5,10,12,15,20};
		texts[] = {"Default","5","10","12","15","20"};
		default = -1;
	};
	
	class AiDifficulty
	{
		title = "AI Skill";
		values[] = {0,1,2,3};
		texts[] = {"Easy","Medium","Hard","Arma"};
		default = 1;
	};
	class StartingResources
	{
		title = "Starting resources";
		values[] = {-1,0,10,25,52,75,100,500};
		texts[] = {"Default","0","10","25","50","75","100","500"};
		default = -1;
	};
	class IncomeRate
	{
		title = "Income Rate (Requisition points every x minutes)";
		values[] = {-1,60,120,300,600};
		texts[] = {"Default","1 minute","2 minutes","5 minutes","10 minutes"};
		default = -1;
	};
	class Debug
	{
		title = "Debug Mode";
		values[] = {0,1};
		texts[] = {"False","True"};
		default = 0;
	};
};


class CfgDebriefing
{
	class WestMin
	{
		title = "BLUFOR MINOR VICTORY";
		subtitle = "";
		description = "";
		pictureColor[] = {};
	};
	class WestMaj
	{
		title = "BLUFOR MAJOR VICTORY";
		subtitle = "";
		description = "";
		pictureColor[] = {};
	};
	class WestTot
	{
		title = "BLUFOR TOTAL VICTORY";
		subtitle = "";
		description = "";
		pictureColor[] = {};
	};

	class EastMin
	{
		title = "OPFOR MINOR VICTORY";
		subtitle = "";
		description = "";
		pictureColor[] = {};
	};
	class EastMaj
	{
		title = "OPFOR MAJOR VICTORY";
		subtitle = "";
		description = "";
		pictureColor[] = {};
	};
	class EastTot
	{
		title = "OPFOR TOTAL VICTORY";
		subtitle = "";
		description = "";
		pictureColor[] = {};
	};

	class IndMin
	{
		title = "INDEPENDENT MINOR VICTORY";
		subtitle = "";
		description = "";
		pictureColor[] = {};
	};
	class IndMaj
	{
		title = "INDEPENDENT MAJOR VICTORY";
		subtitle = "";
		description = "";
		pictureColor[] = {};
	};
	class IndTot
	{
		title = "INDEPENDENT TOTAL VICTORY";
		subtitle = "";
		description = "";
		pictureColor[] = {};
	};
	class Tie
	{
		title = "TIE";
		subtitle = "";
		description = "";
		pictureColor[] = {};
	};
};