class Recon_O : BattalionBase
{
	name = "CSAT Recon Battalion";
	faction = "CSAT";
	side = 0;
	flag = "\a3\Data_f\Flags\flag_CSAT_co.paa";
	icon = "\a3\Data_f\cfgFactionClasses_OPF_ca.paa";

	class hqElement
	{
		name = "O_hqSquad";
	}

	class combatElement
	{
		class O_InfSquad
		{
			count = 12;
			skill = 0.4;
			cost = 12;
		}
		class O_ReconTeam
		{
			count = 10;
			skill = 0.6;
			cost = 10;
		}
		class O_AtTeam
		{
			count = 10;
			skill = 0.6;
			cost = 12;
		}
		class O_ViperSquad
		{
			count = 2;
			skill = 0.9;
			cost = 22;
		}
		class O_Recon_Motorized
		{
			count = 5;
			skill = 0.6;
			cost = 16;
		}
		class O_SniperTeam
		{
			count = 2;
			skill = 0.9;
			cost = 6;
		}
		class O_Kamysh
		{
			count = 4;
			skill = 0.4;
			cost = 35;
		}
		class O_Sochor
		{
			count = 1;
			skill = 0.4;
			cost = 55;
		}

		// Later
		// TODO: Transport vehicle
		// TODO: Repair vehicle
	}
	class Reserves
	{
		modifier = 0.8;
		skill = 0.4;
		units[] = 
		{
            "O_soldier_SL_F",
            "O_soldier_F",
            "O_soldier_LAT_F",
            "O_soldier_LAT_F",
            "O_soldier_AT_F",
            "O_soldier_M_F",
            "O_soldier_TL_F",
            "O_soldier_AR_F",
            "O_soldier_A_F",
            "O_medic_F"
		};
	}
}

// Guard battalion
class Infantry_O : BattalionBase
{
	name = "CSAT Guard Infantry Battalion";
	faction = "CSAT";
	side = 0;
	flag = "\a3\Data_f\Flags\flag_CSAT_co.paa";
	icon = "\a3\Data_f\cfgFactionClasses_OPF_ca.paa";

	class hqElement
	{
		name = "O_hqSquad";
	}

	class combatElement
	{
		class O_InfSquad_Urban
		{
			count = 14;
			skill = 0.4;
			cost = 12;
		}
		class O_UrbanTeam
		{
			count = 6;
			skill = 0.4;
			cost = 8;
		}
		class O_UrbanSquad
		{
			count = 3;
			skill = 0.3;
			cost = 15;
		}
		class O_AtTeamUrban
		{
			count = 10;
			skill = 0.6;
			cost = 12;
		}
		class O_Patrol_Motorized
		{
			count = 2;
			skill = 0.4;
			cost = 22;
		}
		class O_Marid
		{
			count = 2;
			skill = 0.4;
			cost = 27;
		}
		class O_Kamysh
		{
			count = 2;
			skill = 0.4;
			cost = 35;
		}
		class O_Sochor
		{
			count = 2;
			skill = 0.2;
			cost = 55;
		}

		// Later
		// TODO: Transport vehicle
		// TODO: Repair vehicle
	}
	class Reserves
	{
		modifier = 1.2;
		skill = 0.3;
		units[] = 
		{
			// Urban variants
            "O_soldierU_SL_F",
            "O_soldierU_F",
            "O_soldierU_LAT_F",
            "O_soldierU_LAT_F",
            "O_soldierU_AT_F",
            "O_soldierU_M_F",
            "O_soldierU_TL_F",
            "O_soldierU_AR_F",
            "O_soldierU_A_F",
            "O_soldierU_medic_F"
		};
	}
}

class Tank_O : BattalionBase
{
	name = "CSAT Tank Battalion";
	faction = "CSAT";
	side = 0;
	flag = "\a3\Data_f\Flags\flag_CSAT_co.paa";
	icon = "\a3\Data_f\cfgFactionClasses_OPF_ca.paa";

	class hqElement
	{
		name = "O_hqSquad";
	}

	class combatElement
	{
		class O_InfSquad
		{
			count = 12;
			skill = 0.4;
			cost = 12;
		}
		class O_ReconTeam
		{
			count = 4;
			skill = 0.6;
			cost = 10;
		}
		class O_AtTeam
		{
			count = 6;
			skill = 0.4;
			cost = 12;
		}
		class O_Marid
		{
			count = 2;
			skill = 0.4;
			cost = 28;
		}
		class O_Varsuk
		{
			count = 1;
			skill = 0.4;
			cost = 45;
		}
		class O_Angara
		{
			count = 1;
			skill = 0.4;
			cost = 55;
		}
		class O_Angara_K
		{
			count = 1;
			skill = 0.4;
			cost = 60;
		}
		class O_Sochor
		{
			count = 1;
			skill = 0.4;
			cost = 55;
		}

		// Later
		// TODO: Transport vehicle
		// TODO: Repair vehicle
	}
	class Reserves
	{
		modifier = 1;
		skill = 0.3;
		units[] = 
		{
            "O_soldier_SL_F",
            "O_soldier_F",
            "O_soldier_LAT_F",
            "O_soldier_LAT_F",
            "O_soldier_AT_F",
            "O_soldier_M_F",
            "O_soldier_TL_F",
            "O_soldier_AR_F",
            "O_soldier_A_F",
            "O_medic_F"
		};
	}
}


